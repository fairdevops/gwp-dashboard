var datasource_url = "http://localhost/BuddhikaR/GWP_Dashboard/";
var previous_month = "";

$(document).ready(function () {
	var elem = document.querySelector('.js-switch');
	var init = new Switchery(elem, {
		color: '#faab43',
		secondaryColor: '#fC73d0',
		jackColor: '#fcf45e',
		jackSecondaryColor: '#c8ff77'
	});

	var switchery = new Switchery("#switch", {
		color: '#faab43',
		secondaryColor: '#fC73d0',
		jackColor: '#fcf45e',
		jackSecondaryColor: '#c8ff77'
	});

	var document_height = $(document).height() - 50;
	var main_chart_height = (document_height * 70) / 100;
	var month_chart_height = (document_height * 30) / 100;

	$("#main_chart_wrap").height(main_chart_height);
	$("#month_chart_wrap").height(month_chart_height);
	$("#donut_wrap").height(month_chart_height);
	$("#donut_wrap1").height(month_chart_height);
	$("#gauge_wrap").height(month_chart_height);
	$("#branch_chart_1").height((document_height - 70) / 2);
	$("#branch_chart_2").height((document_height - 70) / 2);

	//var chart = AmCharts.makeChart("main_chart_wrap", {
	var main_chart = AmCharts.makeChart("main_chart_wrap", {
		"theme": "light",
		"type": "serial",
		"dataProvider": [{
			"month": "January",
			"target": 5,
			"achieved": 4.2,
			"cumulated_target": 100,
			"cumulated_achieved": 70,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "February",
			"target": 5,
			"achieved": 3.1,
			"cumulated_target": 200,
			"cumulated_achieved": 145,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "March",
			"target": 5,
			"achieved": 2.9,
			"cumulated_target": 300,
			"cumulated_achieved": 225,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "April",
			"target": 5,
			"achieved": 2.3,
			"cumulated_target": 400,
			"cumulated_achieved": 315,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "May",
			"target": 5,
			"achieved": 2.1,
			"cumulated_target": 500,
			"cumulated_achieved": 382,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "June",
			"target": 5,
			"achieved": 3.1,
			"cumulated_target": 600,
			"cumulated_achieved": 462,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "July",
			"target": 5,
			"achieved": 3.2,
			"cumulated_target": 700,
			"cumulated_achieved": 537,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "August",
			"target": 5,
			"achieved": 3.5,
			"cumulated_target": 800,
			"cumulated_achieved": 622,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "September",
			"target": 5,
			"achieved": 2.5,
			"cumulated_target": 900,
			"cumulated_achieved": 702,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "October",
			"target": 5,
			"achieved": 3.4,
			"cumulated_target": 1000,
			"cumulated_achieved": 800,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "November",
			"target": 5,
			"achieved": 0,
			"cumulated_target": 1100,
			//"cumulated_achieved": 0,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "December",
			"target": 5,
			"achieved": 0,
			"cumulated_target": 1200,
			//"cumulated_achieved": 0,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }],
		"valueAxes": [{
			"unit": "/=",
			"position": "left",
			"title": "GWP",
			"minimum": 0,
			"gridAlpha": 0,
                }, {
			"id": "a1",
			"unit": "/=",
			"position": "right",
			"title": "Cumulated GWP",
			"minimum": 0,
			"gridAlpha": 0,
                }],

		"startDuration": 1,
		"graphs": [{
			"balloonText": "Target GWP in [[category]] : <b>[[value]]</b>",
			"fillAlphas": 0.9,
			"lineAlpha": 0.2,
			"title": "2004",
			"type": "column",
			"valueField": "target",
			"colorField": "target_color"
                }, {
			"balloonText": "Achieved GWP in [[category]] : <b>[[value]]</b>",
			"fillAlphas": 0.9,
			"lineAlpha": 0.2,
			"title": "2005",
			"type": "column",
			"clustered": false,
			"columnWidth": 0.75,
			"valueField": "achieved",
			"colorField": "achieved_color"
                }, {
			id: "g2",
			valueField: "cumulated_target",
			classNameField: "bulletClass",
			title: "Achievement",
			type: "line",
			valueAxis: "a1",
			lineColor: "#000000",
			lineThickness: 1,
			legendValueText: "[[description]]/[[value]]",
			descriptionField: "townName",
			bullet: "triangleDown",
			bulletSize: 10,
			bulletSizeField: "townSize",
			bulletBorderColor: "#ff6600",
			bulletBorderAlpha: 1,
			bulletBorderThickness: 2,
			bulletColor: "#ffcfaf",
			labelText: "[[townName2]]",
			labelPosition: "right",
			balloonText: " Achieved :[[value]]",
			showBalloon: true,
			animationPlayed: true,
              }, , {
			id: "g3",
			valueField: "cumulated_achieved",
			classNameField: "bulletClass",
			title: "Achievement",
			type: "line",
			valueAxis: "a1",
			lineColor: "#000000",
			lineThickness: 1,
			legendValueText: "[[description]]/[[value]]",
			descriptionField: "townName",
			bullet: "round",
			bulletSize: 10,
			bulletSizeField: "townSize",
			bulletBorderColor: "#ff6600",
			bulletBorderAlpha: 1,
			bulletBorderThickness: 2,
			bulletColor: "#ffcfaf",
			labelText: "[[townName2]]",
			labelPosition: "right",
			balloonText: " Achieved :[[value]]",
			showBalloon: true,
			animationPlayed: true,
              }],
		"plotAreaFillAlphas": 0.1,
		"categoryField": "month",
		"categoryAxis": {
			"gridPosition": "start",
			"dashLength": 6,
			"gridPosition": "middle",
			"gridColor": "red"
		},
		"export": {
			"enabled": true
		}

	}); // End of main chart

	jQuery.ajax({
        url: datasource_url + 'main_chart.php',
        success: function (result) {
			json_result = JSON.parse(result)
			for(val in json_result ){
				json_result[val]["target_color"] = "#d9e4eb";
				json_result[val]["achieved_color"] = "#47bac1";
			}
            main_chart.dataProvider = json_result;
			main_chart.validateData();
			//console.log(result);
        },
        async: false
    });
	
	var month_chart = AmCharts.makeChart("month_chart_wrap", {
		"type": "serial",
		"theme": "light",
		"legend": {
			"horizontalGap": 10,
			"maxColumns": 1,
			"position": "right",
			"useGraphSettings": true,
			"markerSize": 10
		},
		"dataProvider": [{
			"year": "Agent",
			"motor": 2.1,
			"non_motor": 2.6,
			"new_business": 2.1,
			"renewal": 0.3,
			"color_motor": "#47bac1",
			"color_non_motor": "#d9e4eb",
                }, {
			"year": "Broker",
			"motor": 1.9,
			"non_motor": 2.7,
			"new_business": 2.1,
			"renewal": 0.3,
			"color_motor": "#47bac1",
			"color_non_motor": "#d9e4eb",
                }, {
			"year": "KAM",
			"motor": 1.5,
			"non_motor": 2.5,
			"new_business": 2.1,
			"renewal": 0.3,
			"color_motor": "#47bac1",
			"color_non_motor": "#d9e4eb",
                }, {
			"year": "Affinity",
			"motor": 1.9,
			"non_motor": 2.2,
			"new_business": 2.1,
			"renewal": 0.3,
			"color_motor": "#47bac1",
			"color_non_motor": "#d9e4eb",
                }],
		"valueAxes": [{
			"stackType": "regular",
			"axisAlpha": 0.5,
			"gridAlpha": 0
                }],
		"graphs": [{
			"balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
			"fillAlphas": 0.8,
			"labelText": "[[value]]",
			"lineAlpha": 0.3,
			"title": "Motor",
			"type": "column",
			"color": "#000000",
			"valueField": "motor",
			"colorField": "color_motor",

                }, {
			"balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
			"fillAlphas": 0.8,
			"labelText": "[[value]]",
			"lineAlpha": 0.3,
			"title": "Non Motor",
			"type": "column",
			"color": "#000000",
			"valueField": "non_motor",
			"colorField": "color_non_motor"
                }],
		"rotate": true,
		"categoryField": "year",
		"legend": "",
		"categoryAxis": {
			"gridPosition": "start",
			"axisAlpha": 0,
			"gridAlpha": 0,
			"position": "left"
		},
		"export": {
			"enabled": true
		}
	}); // End of month chart

	var gauge_chart = AmCharts.makeChart("donut_wrap", {

		"type": "pie",
		//"theme": "light",
		"dataProvider": [{
			"title": "Achieved",
			"value": 9899,
			"color": "#47bac1"
              }, {
			"title": "Remaining",
			"value": 4852,
			"color": "#d9e4eb"
              }],
		//"titleField": "title",
		"valueField": "value",
		"colorField": "color",
		"labelRadius": 5,
		"allLabels": [{
			"text": "34%",
			"color": "#f40000",
			"size": 30,
			"align": "center",
			"y": "38%"
              }],

		"radius": "42%",
		"innerRadius": "80%",
		"labelText": "[[title]]",
		"export": {
			"enabled": true
		}
	}); // End of donut chart

	var gauge_chart = AmCharts.makeChart("donut_wrap1", {

		"type": "pie",
		//"theme": "light",
		"dataProvider": [{
			"title": "Achieved",
			"value": 9899,
			"color": "#47bac1"
              }, {
			"title": "Remaining",
			"value": 4852,
			"color": "#d9e4eb"
              }],
		//"titleField": "title",
		"valueField": "value",
		"colorField": "color",
		"labelRadius": 5,
		"allLabels": [{
			"text": "34%",
			"color": "#f40000",
			"size": 30,
			"align": "center",
			"y": "38%"
              }],

		"radius": "42%",
		"innerRadius": "80%",
		"labelText": "[[title]]",
		"export": {
			"enabled": true
		},
		"startEffect": "elastic"
	}); // End of donut1 chart

	var gauge_chart = AmCharts.makeChart("gauge_wrap", {
		"type": "gauge",
		"theme": "light",
		"axes": [{
			"axisThickness": 0,
			"lineThickness": 25,
			"axisAlpha": 0.1,
			"tickAlpha": 10,
			"valueInterval": 1,
			"bands": [{
				"color": "#84b761",
				"endValue": 1,
				"startValue": 0
                    }, {
				"color": "#fdd400",
				"endValue": 2,
				"startValue": 1
                    }, {
				"color": "#cc4748",
				"endValue": 3,
				"innerRadius": "95%",
				"startValue": 2
                    }, {
				"color": "#84b761",
				"endValue": 4,
				"innerRadius": "95%",
				"startValue": 3
                    }, {
				"color": "#fdd400",
				"endValue": 5,
				"innerRadius": "95%",
				"startValue": 4
                    }, {
				"color": "#cc4748",
				"endValue": 6,
				"innerRadius": "95%",
				"startValue": 5
                    }, {
				"color": "#84b761",
				"endValue": 7,
				"innerRadius": "95%",
				"startValue": 6
                    }, {
				"color": "#fdd400",
				"endValue": 8,
				"innerRadius": "95%",
				"startValue": 7
                    }, {
				"color": "#cc4748",
				"endValue": 9,
				"innerRadius": "95%",
				"startValue": 8
                    }, {
				"color": "#84b761",
				"endValue": 10,
				"innerRadius": "95%",
				"startValue": 9
                    }, {
				"color": "#fdd400",
				"endValue": 11,
				"innerRadius": "95%",
				"startValue": 10
                    }, {
				"color": "#cc4748",
				"endValue": 12,
				"innerRadius": "95%",
				"startValue": 11
                    }],
			"bottomText": "1B / 6.3B ",
			"bottomTextYOffset": -20,
			"endValue": 12
                  }],
		"arrows": [{}],
		"export": {
			"enabled": true
		}
	}); // End of gauge chart
	//gauge_chart.arrows[ 0 ].setValue( 10 );
	//gauge_chart.validateData();

	var branch_chart_1 = AmCharts.makeChart("branch_chart_1", {
		"theme": "light",
		"type": "serial",
		"dataProvider": [{
			"month": "January",
			"target": 5,
			"achieved": 4.2,
			"cumulated_target": 100,
			"cumulated_achieved": 70,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "February",
			"target": 5,
			"achieved": 3.1,
			"cumulated_target": 200,
			"cumulated_achieved": 145,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "March",
			"target": 5,
			"achieved": 2.9,
			"cumulated_target": 300,
			"cumulated_achieved": 225,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "April",
			"target": 5,
			"achieved": 2.3,
			"cumulated_target": 400,
			"cumulated_achieved": 315,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "May",
			"target": 5,
			"achieved": 2.1,
			"cumulated_target": 500,
			"cumulated_achieved": 382,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "June",
			"target": 5,
			"achieved": 3.1,
			"cumulated_target": 600,
			"cumulated_achieved": 462,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "July",
			"target": 5,
			"achieved": 3.2,
			"cumulated_target": 700,
			"cumulated_achieved": 537,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "August",
			"target": 5,
			"achieved": 3.5,
			"cumulated_target": 800,
			"cumulated_achieved": 622,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "September",
			"target": 5,
			"achieved": 2.5,
			"cumulated_target": 900,
			"cumulated_achieved": 702,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "October",
			"target": 5,
			"achieved": 3.4,
			"cumulated_target": 1000,
			"cumulated_achieved": 800,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "November",
			"target": 5,
			"achieved": 0,
			"cumulated_target": 1100,
			"cumulated_achieved": 75,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "December",
			"target": 5,
			"achieved": 0,
			"cumulated_target": 1200,
			"cumulated_achieved": 70,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "January",
			"target": 5,
			"achieved": 4.2,
			"cumulated_target": 100,
			"cumulated_achieved": 70,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "February",
			"target": 5,
			"achieved": 3.1,
			"cumulated_target": 200,
			"cumulated_achieved": 145,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "March",
			"target": 5,
			"achieved": 2.9,
			"cumulated_target": 300,
			"cumulated_achieved": 225,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "April",
			"target": 5,
			"achieved": 2.3,
			"cumulated_target": 400,
			"cumulated_achieved": 315,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "May",
			"target": 5,
			"achieved": 2.1,
			"cumulated_target": 500,
			"cumulated_achieved": 382,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "June",
			"target": 5,
			"achieved": 3.1,
			"cumulated_target": 600,
			"cumulated_achieved": 462,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "July",
			"target": 5,
			"achieved": 3.2,
			"cumulated_target": 700,
			"cumulated_achieved": 537,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "August",
			"target": 5,
			"achieved": 3.5,
			"cumulated_target": 800,
			"cumulated_achieved": 622,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "September",
			"target": 5,
			"achieved": 2.5,
			"cumulated_target": 900,
			"cumulated_achieved": 702,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "October",
			"target": 5,
			"achieved": 3.4,
			"cumulated_target": 1000,
			"cumulated_achieved": 800,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "November",
			"target": 5,
			"achieved": 0,
			"cumulated_target": 1100,
			"cumulated_achieved": 75,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "December",
			"target": 5,
			"achieved": 0,
			"cumulated_target": 1200,
			"cumulated_achieved": 70,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }],
		"valueAxes": [{
			"unit": "Mil",
			"position": "left",
			"title": "GWP",
			"minimum": 0,
			"gridAlpha": 0,
                }, {
			"id": "a1",
			"unit": "Mil",
			"position": "right",
			"title": "Cumulated GWP",
			"minimum": 0,
			"gridAlpha": 0,
                }],

		"startDuration": 1,
		"graphs": [{
			"balloonText": "Target GWP in [[category]] : <b>[[value]]</b>",
			"fillAlphas": 0.9,
			"lineAlpha": 0.2,
			"title": "2004",
			"type": "column",
			"valueField": "target",
			"colorField": "target_color"
                }, {
			"balloonText": "Achieved GWP in [[category]] : <b>[[value]]</b>",
			"fillAlphas": 0.9,
			"lineAlpha": 0.2,
			"title": "2005",
			"type": "column",
			"clustered": false,
			"columnWidth": 0.75,
			"valueField": "achieved",
			"colorField": "achieved_color"
                }],
		"plotAreaFillAlphas": 0.1,
		"categoryField": "month",
		"categoryAxis": {
			"gridPosition": "start",
			"dashLength": 6,
			"gridPosition": "middle",
			"gridColor": "red"
		},
		"export": {
			"enabled": true
		}

	}); // End of branch chart

	var branch_chart_2 = AmCharts.makeChart("branch_chart_2", {
		"theme": "light",
		"type": "serial",
		"dataProvider": [{
			"month": "January",
			"target": 5,
			"achieved": 4.2,
			"cumulated_target": 100,
			"cumulated_achieved": 70,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "February",
			"target": 5,
			"achieved": 3.1,
			"cumulated_target": 200,
			"cumulated_achieved": 145,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "March",
			"target": 5,
			"achieved": 2.9,
			"cumulated_target": 300,
			"cumulated_achieved": 225,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "April",
			"target": 5,
			"achieved": 2.3,
			"cumulated_target": 400,
			"cumulated_achieved": 315,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "May",
			"target": 5,
			"achieved": 2.1,
			"cumulated_target": 500,
			"cumulated_achieved": 382,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "June",
			"target": 5,
			"achieved": 3.1,
			"cumulated_target": 600,
			"cumulated_achieved": 462,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "July",
			"target": 5,
			"achieved": 3.2,
			"cumulated_target": 700,
			"cumulated_achieved": 537,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "August",
			"target": 5,
			"achieved": 3.5,
			"cumulated_target": 800,
			"cumulated_achieved": 622,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "September",
			"target": 5,
			"achieved": 2.5,
			"cumulated_target": 900,
			"cumulated_achieved": 702,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "October",
			"target": 5,
			"achieved": 3.4,
			"cumulated_target": 1000,
			"cumulated_achieved": 800,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "November",
			"target": 5,
			"achieved": 0,
			"cumulated_target": 1100,
			"cumulated_achieved": 75,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "December",
			"target": 5,
			"achieved": 0,
			"cumulated_target": 1200,
			"cumulated_achieved": 70,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "January",
			"target": 5,
			"achieved": 4.2,
			"cumulated_target": 100,
			"cumulated_achieved": 70,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "February",
			"target": 5,
			"achieved": 3.1,
			"cumulated_target": 200,
			"cumulated_achieved": 145,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "March",
			"target": 5,
			"achieved": 2.9,
			"cumulated_target": 300,
			"cumulated_achieved": 225,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "April",
			"target": 5,
			"achieved": 2.3,
			"cumulated_target": 400,
			"cumulated_achieved": 315,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "May",
			"target": 5,
			"achieved": 2.1,
			"cumulated_target": 500,
			"cumulated_achieved": 382,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "June",
			"target": 5,
			"achieved": 3.1,
			"cumulated_target": 600,
			"cumulated_achieved": 462,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "July",
			"target": 5,
			"achieved": 3.2,
			"cumulated_target": 700,
			"cumulated_achieved": 537,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "August",
			"target": 5,
			"achieved": 3.5,
			"cumulated_target": 800,
			"cumulated_achieved": 622,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "September",
			"target": 5,
			"achieved": 2.5,
			"cumulated_target": 900,
			"cumulated_achieved": 702,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "October",
			"target": 5,
			"achieved": 3.4,
			"cumulated_target": 1000,
			"cumulated_achieved": 800,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "November",
			"target": 5,
			"achieved": 0,
			"cumulated_target": 1100,
			"cumulated_achieved": 75,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }, {
			"month": "December",
			"target": 5,
			"achieved": 0,
			"cumulated_target": 1200,
			"cumulated_achieved": 70,
			"target_color": "#d9e4eb",
			"achieved_color": "#47bac1"
                }],
		"valueAxes": [{
			"unit": "Mil",
			"position": "left",
			"title": "GWP",
			"minimum": 0,
			"gridAlpha": 0,
                }, {
			"id": "a1",
			"unit": "Mil",
			"position": "right",
			"title": "Cumulated GWP",
			"minimum": 0,
			"gridAlpha": 0,
                }],

		"startDuration": 1,
		"graphs": [{
			"balloonText": "Target GWP in [[category]] : <b>[[value]]</b>",
			"fillAlphas": 0.9,
			"lineAlpha": 0.2,
			"title": "2004",
			"type": "column",
			"valueField": "target",
			"colorField": "target_color"
                }, {
			"balloonText": "Achieved GWP in [[category]] : <b>[[value]]</b>",
			"fillAlphas": 0.9,
			"lineAlpha": 0.2,
			"title": "2005",
			"type": "column",
			"clustered": false,
			"columnWidth": 0.75,
			"valueField": "achieved",
			"colorField": "achieved_color"
                }],
		"plotAreaFillAlphas": 0.1,
		"categoryField": "month",
		"categoryAxis": {
			"gridPosition": "start",
			"dashLength": 6,
			"gridPosition": "middle",
			"gridColor": "red"
		},
		"export": {
			"enabled": true
		}

	}); // End of branch chart 2

	main_chart.addListener("clickGraphItem", handleClick);

	function handleClick(event) {
		//alert(event.item.category + ": " + event.item.values.value);
		//console.log(main_chart.dataProvider);
		//                main_chart.dataProvider = [{
		//                    achieved: 4.2,
		//                    achieved_color: "#47bac1",
		//                    month: "January",
		//                    cumulated_achieved: 70,
		//                    cumulated_target: 100,
		//                    target: 5,
		//                    target_color: "#d9e4eb"                             
		//                }];
		if (previous_month) {
			main_chart.dataProvider[getMonthNumberFromMonth(previous_month)].achieved_color = "#47bac1";
		}
		main_chart.dataProvider[getMonthNumberFromMonth(event.item.category)].achieved_color = "red";

		previous_month = event.item.category;

		main_chart.validateData();
	}

	$(".modal-transparent").on('show.bs.modal', function () {
		setTimeout(function () {
			$(".modal-backdrop").addClass("modal-backdrop-transparent");
		}, 0);
	});
	$(".modal-transparent").on('hidden.bs.modal', function () {
		$(".modal-backdrop").addClass("modal-backdrop-transparent");
	});

	$(".modal-fullscreen").on('show.bs.modal', function () {
		setTimeout(function () {
			$(".modal-backdrop").addClass("modal-backdrop-fullscreen");
		}, 0);
	});
	$(".modal-fullscreen").on('hidden.bs.modal', function () {
		$(".modal-backdrop").addClass("modal-backdrop-fullscreen");
	});

	$("#branch_popup").on('click', function () {
		$('#modal-fullscreen').modal('show');
	});

	$("#switch").on('change', function () {
		//var value = Math.round(Math.random() * 10);
		gauge_chart.arrows[0].setValue(10);
		/*if (gauge_chart) {
			if (gauge_chart.arrows) {
				if (gauge_chart.arrows[0]) {
					if (gauge_chart.arrows[0].setValue) {
						gauge_chart.arrows[0].setValue(value);
						gauge_chart.axes[0].setBottomText(value + " bil");
					}
				}
			}
		}*/

		if ($("#switch").is(":checked") == true) {
			$("#month_chart_type_text").html("New Business / Renewal");
		} else {
			$("#month_chart_type_text").html("Motor / Non Motor");
		}
	});

}); // End of document.ready


function getMonthNumberFromMonth(month) {
	var month_number;
	switch (month) {
	case "January":
		month_number = 0;
		break;
	case "February":
		month_number = 1;
		break;
	case "March":
		month_number = 2;
		break;
	case "April":
		month_number = 3;
		break;
	case "May":
		month_number = 4;
		break;
	case "June":
		month_number = 5;
		break;
	case "July":
		month_number = 6;
		break;
	case "August":
		month_number = 7;
		break;
	case "September":
		month_number = 8;
		break;
	case "October":
		month_number = 9;
		break;
	case "November":
		month_number = 10;
		break;
	case "December":
		month_number = 11;
		break;
	}
	return month_number;
}