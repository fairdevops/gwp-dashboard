var http = location.protocol;
var slashes = http.concat("//");
var host = slashes.concat(window.location.hostname);

// var datasource_url = host+"/dashboard/gwp_dashboard/backend/";

var datasource_url = host+"/fairfirst_gwp_beta/backend/";

// alert(datasource_url);
var current_month = "All";
var selected_channel = "All";
var selected_channel_for_branch = "";

var main_chart_data_source = null;
var circle_chart_data_source = null;
var drilldown_chart_data_source = null;
var branch_chart_data_source = null;
var portfolio_chart_data_source = null;

var current_charm_button_div = "charm_all_button";

var total_dragged = false;

$(document).ready(function () {
    $( "#branch_breakdown_popup" ).draggable({
});
//    $( "#agency_breakdown_popup" ).draggable({
//        containment : [280,125,500,500]
//    });
	// Get Data From Servers
	reloadData();

	// function to refresh main chart data
	function refreshMainChart() {
		for (val in main_chart_data_source[selected_channel]) {
			main_chart_data_source[selected_channel][val]["target_color"] = "#80daff";
			main_chart_data_source[selected_channel][val]["achieved_color"] = "#0090ff";
            main_chart_data_source[selected_channel][val]["percentage"] = ( (main_chart_data_source[selected_channel][val]["achieved"] *  100)  / main_chart_data_source[selected_channel][val]["target"] ).toFixed(2);
		}

		main_chart.dataProvider = main_chart_data_source[selected_channel];
		if (current_month != "All") {
			main_chart.dataProvider[getMonthNumberFromMonth(current_month)].target_color = "#c3bb39";
			main_chart.dataProvider[getMonthNumberFromMonth(current_month)].achieved_color = "#fff000";

		}
		main_chart.validateData();
	}
    
    // function to refresh portfolio chart data
    function refreshPortfolioChart() {
        
		for (val in portfolio_chart_data_source[current_month]) {
            if(portfolio_chart_data_source[current_month][val]["portfolio"] == "Motor"){
                portfolio_chart_data_source[current_month][val]["target_color"] = "#ffe7bf";
			    portfolio_chart_data_source[current_month][val]["achieved_color"] = "#ffaf2a";
            } else if(portfolio_chart_data_source[current_month][val]["portfolio"] == "Fire"){
                portfolio_chart_data_source[current_month][val]["target_color"] = "#fdb0a8";
			    portfolio_chart_data_source[current_month][val]["achieved_color"] = "#ff321c";
            } else if(portfolio_chart_data_source[current_month][val]["portfolio"] == "Casualty"){
                portfolio_chart_data_source[current_month][val]["target_color"] = "#c7cbef";
			    portfolio_chart_data_source[current_month][val]["achieved_color"] = "#7c83c9";
            } else if(portfolio_chart_data_source[current_month][val]["portfolio"] == "Health"){
                portfolio_chart_data_source[current_month][val]["target_color"] = "#c5fdcb";
			    portfolio_chart_data_source[current_month][val]["achieved_color"] = "#00b515";
            } else if(portfolio_chart_data_source[current_month][val]["portfolio"] == "Marine"){
                portfolio_chart_data_source[current_month][val]["target_color"] = "#80daff";
			    portfolio_chart_data_source[current_month][val]["achieved_color"] = "#0090ff";
            } else {
                portfolio_chart_data_source[current_month][val]["target_color"] = "#ffffff";
			    portfolio_chart_data_source[current_month][val]["achieved_color"] = "#000000";
            }
			
            portfolio_chart_data_source[current_month][val]["percentage"] = ( (portfolio_chart_data_source[current_month][val]["achieved"] *  100)  / portfolio_chart_data_source[current_month][val]["target"] ).toFixed(2);
		}

		portfolio_chart.dataProvider = portfolio_chart_data_source[current_month];
//		if (current_month != "All") {
//			main_chart.dataProvider[getMonthNumberFromMonth(current_month)].target_color = "#c3bb39";
//			main_chart.dataProvider[getMonthNumberFromMonth(current_month)].achieved_color = "#fff000";
//
//		}
		portfolio_chart.validateData();
	}
    
	// function to refresh circle chart data
	function refreshCircleCharts() {
		var data_source = circle_chart_data_source[current_month];
		console.log(data_source);
		var percentage = null;
		var text = null;

		percentage = Math.round((data_source["Agency"]["achieved"] * 100) / data_source["Agency"]["target"]);
		text = Math.round(data_source["Agency"]["achieved"] / 1000000) + "mil / " + Math.round(data_source["Agency"]["target"] / 1000000) + "mil"
		$('#chart_agency').empty().removeData().attr('data-text', percentage + '%').attr('data-percent', percentage).attr('data-info', text).circliful();

		percentage = Math.round((data_source["Affinity"]["achieved"] * 100) / data_source["Affinity"]["target"]);
		text = Math.round(data_source["Affinity"]["achieved"] / 1000000) + "mil / " + Math.round(data_source["Affinity"]["target"] / 1000000) + "mil"
		$('#chart_affinity').empty().removeData().attr('data-text', percentage + '%').attr('data-percent', percentage).attr('data-info', text).circliful();

		percentage = Math.round((data_source["KAM"]["achieved"] * 100) / data_source["KAM"]["target"]);
		text = Math.round(data_source["KAM"]["achieved"] / 1000000) + "mil / " + Math.round(data_source["KAM"]["target"] / 1000000) + "mil"
		$('#chart_kam').empty().removeData().attr('data-text', percentage + '%').attr('data-percent', percentage).attr('data-info', text).circliful();

		percentage = Math.round((data_source["Broker"]["achieved"] * 100) / data_source["Broker"]["target"]);
		text = Math.round(data_source["Broker"]["achieved"] / 1000000) + "mil / " + Math.round(data_source["Broker"]["target"] / 1000000) + "mil"
		$('#chart_broker').empty().removeData().attr('data-text', percentage + '%').attr('data-percent', percentage).attr('data-info', text).circliful();
        
        
        percentage = Math.round((data_source["Digital"]["achieved"] * 100) / data_source["Digital"]["target"]);
		text = Math.round(data_source["Digital"]["achieved"] / 1000000) + "mil / " + Math.round(data_source["Digital"]["target"] / 1000000) + "mil"
		$('#chart_digital').empty().removeData().attr('data-text', percentage + '%').attr('data-percent', percentage).attr('data-info', text).circliful();


		percentage = Math.round((data_source["Softlogic"]["achieved"] * 100) / data_source["Softlogic"]["target"]);
		text = Math.round(data_source["Softlogic"]["achieved"] / 1000000) + "mil / " + Math.round(data_source["Softlogic"]["target"] / 1000000) + "mil"
		$('#chart_softlogic').empty().removeData().attr('data-text', percentage + '%').attr('data-percent', percentage).attr('data-info', text).circliful();
        
        var total_percentage = Math.round( ( (parseFloat(data_source["Agency"]["achieved"]) + parseFloat(data_source["Affinity"]["achieved"]) + parseFloat(data_source["Broker"]["achieved"]) + parseFloat(data_source["KAM"]["achieved"]) + parseFloat(data_source["Digital"]["achieved"]) + parseFloat(data_source["Softlogic"]["achieved"])) * 100 ) / ( parseFloat(data_source["Agency"]["target"]) + parseFloat(data_source["Affinity"]["target"]) + parseFloat(data_source["Broker"]["target"]) + parseFloat(data_source["KAM"]["target"]) + parseFloat(data_source["Digital"]["target"]) + parseFloat(data_source["Softlogic"]["target"])) );
        
        if(isNaN(total_percentage)){
            total_percentage = 0;
        }
        
        var total_text_1 = Math.round(( parseFloat(data_source["Agency"]["achieved"]) + parseFloat(data_source["Affinity"]["achieved"]) + parseFloat(data_source["Broker"]["achieved"]) + parseFloat(data_source["KAM"]["achieved"]) +  parseFloat(data_source["Digital"]["achieved"]) +  parseFloat(data_source["Softlogic"]["achieved"])) / 1000000) ;
        
        if(isNaN(total_text_1)){
            total_text_1 = "0mil / ";
        }else{
            total_text_1 = total_text_1 + "mil / ";
        }
        
        var total_text_2 = Math.round((parseFloat(data_source["Agency"]["target"]) + parseFloat(data_source["Affinity"]["target"]) + parseFloat(data_source["Broker"]["target"]) + parseFloat(data_source["KAM"]["target"]) + parseFloat(data_source["Digital"]["target"]) + parseFloat(data_source["Softlogic"]["target"])) / 1000000);
        
        if(isNaN(total_text_2)){
            total_text__2 = "0mil";
        }else{
            total_text_2 = total_text_2 + "mil";
        }
        
        var total_text = total_text_1 + total_text_2;
        //console.log(data_source["Agency"]["achieved"]);
//        percentage = Math.round((data_source["Broker"]["achieved"] * 100) / data_source["Broker"]["target"]);
//		text = Math.round(data_source["Broker"]["achieved"] / 1000000) + "mil / " + Math.round(data_source["Broker"]["target"] / 1000000) + "mil"
		$('#chart_all').empty().removeData().attr('data-text', total_percentage + '%').attr('data-percent', total_percentage).attr('data-info', total_text).circliful();
	}


	// function to refresh drill down chart data
	function refreshDrillDownCharts(channel) {

        var new_business = 0;
        var renewal = 0;
        var motor = 0;
        var non_motor = 0;
        
        if(channel == "All"){
            new_business = parseFloat(drilldown_chart_data_source[current_month]["Agency"]["NewBusiness"]) + parseFloat(drilldown_chart_data_source[current_month]["Affinity"]["NewBusiness"]) + parseFloat(drilldown_chart_data_source[current_month]["Broker"]["NewBusiness"]) + parseFloat(drilldown_chart_data_source[current_month]["KAM"]["NewBusiness"]);
            
            renewal = parseFloat(drilldown_chart_data_source[current_month]["Agency"]["Renewal"]) + parseFloat(drilldown_chart_data_source[current_month]["Affinity"]["Renewal"]) + parseFloat(drilldown_chart_data_source[current_month]["Broker"]["Renewal"]) + parseFloat(drilldown_chart_data_source[current_month]["KAM"]["Renewal"]);
            
            motor = parseFloat(drilldown_chart_data_source[current_month]["Agency"]["Motor"]) + parseFloat(drilldown_chart_data_source[current_month]["Affinity"]["Motor"]) + parseFloat(drilldown_chart_data_source[current_month]["Broker"]["Motor"]) + parseFloat(drilldown_chart_data_source[current_month]["KAM"]["Motor"]);
            
            non_motor = parseFloat(drilldown_chart_data_source[current_month]["Agency"]["NonMotor"]) + parseFloat(drilldown_chart_data_source[current_month]["Affinity"]["NonMotor"]) + parseFloat(drilldown_chart_data_source[current_month]["Broker"]["NonMotor"]) + parseFloat(drilldown_chart_data_source[current_month]["KAM"]["NonMotor"]);
            
        }else{
            new_business = drilldown_chart_data_source[current_month][channel]["NewBusiness"];
            renewal = drilldown_chart_data_source[current_month][channel]["Renewal"];
            motor = drilldown_chart_data_source[current_month][channel]["Motor"];
            non_motor = drilldown_chart_data_source[current_month][channel]["NonMotor"];
        }
   
        
		var nr_data_source = [{
				"category": "New Business",
				"gwp": new_business,
				"color": "#FF6600"
							},
			{
				"category": "Renewal",
				"gwp": renewal,
				"color": "#FF0F00"
							}];
		drilldown_chart_newbusiness_renewal.dataProvider = nr_data_source;
		drilldown_chart_newbusiness_renewal.validateData();

		var mn_data_source = [{
				"category": "Motor",
				"gwp": motor,
				"color": "#FF6600"
							},
			{
				"category": "Non Motor",
				"gwp": non_motor,
				"color": "#FF0F00"
							}];
		drilldown_chart_motor_nonmotor.dataProvider = mn_data_source;
		drilldown_chart_motor_nonmotor.validateData();
	}


	// function to refresh branch down chart data
	function refreshBranchCharts(channel) {
        selected_channel_for_branch = channel;
        var data_source = null;
        
        if(channel == "All"){
            //console.log(branch_chart_data_source[current_month]);
            data_source = branch_chart_data_source[current_month][channel];
            
        }else{
            data_source = branch_chart_data_source[current_month][channel];    
        }
		
        
        //console.log(channel);
        //console.log(data_source);
        
		var number_of_branches_in_datasource = data_source.length;

		if (number_of_branches_in_datasource % 2 != 0) {
			// Odd numbers just devide by two
			data_source.push({
				branch: "",
				target: 0,
				gwp: 0
			});
		}

		var number_of_branches_in_chart = data_source.length / 2;

		var branch_1_datasource = [];
		var branch_2_datasource = [];
		for (val in data_source) {
			data_source[val]["target_color"] = "#80cbff";
			data_source[val]["achieved_color"] = "#47bac1";
            data_source[val]["percentage"] = ( (data_source[val]["gwp"] *  100)  / data_source[val]["target"] ).toFixed(2);
            /*console.log( data_source[val]["gwp"] + " / " + data_source[val]["target"] + " : " + ( (data_source[val]["gwp"] *  100)  / data_source[val]["target"]).toFixed(2) );*/
			if (val < number_of_branches_in_chart) {
				branch_1_datasource.push(data_source[val]);
			} else {
				branch_2_datasource.push(data_source[val]);
			}

		}

        

		branch_chart_1.categoryAxis.gridCount = number_of_branches_in_chart;
		branch_chart_2.categoryAxis.gridCount = number_of_branches_in_chart;
		
		branch_chart_1.dataProvider = branch_1_datasource;
		branch_chart_2.dataProvider = branch_2_datasource;
		branch_chart_1.validateData();
		branch_chart_2.validateData();


	}
    
    // function to refresh branch drill down chart data
    
    function refreshBranchDrillDownChart(branch_code){
        
            var agency_data = null;
            var broker_data = null;
            var kam_data = null;
            var affinity_data = null;
            var digital_data = null;
            var softlogic_data = null;
            var branch_drilldown_data_array = [];

            var i = 0;
            while(branch_chart_data_source[current_month]["Agency"][i]["branch"] != branch_code ){            
                i++;
            }
            agency_data = branch_chart_data_source[current_month]["Agency"][i];
            agency_data["channel"] = "Agency";

            var i = 0;
            while(branch_chart_data_source[current_month]["Broker"][i]["branch"] != branch_code ){            
                i++;
            }
            broker_data = branch_chart_data_source[current_month]["Broker"][i];
            broker_data["channel"] = "Broker";

            var i = 0;
            while(branch_chart_data_source[current_month]["KAM"][i]["branch"] != branch_code ){            
                i++;
            }
            kam_data = branch_chart_data_source[current_month]["KAM"][i];
            kam_data["channel"] = "KAM";

            var i = 0;
            while(branch_chart_data_source[current_month]["Affinity"][i]["branch"] != branch_code ){            
                i++;
            }
            affinity_data = branch_chart_data_source[current_month]["Affinity"][i];
            affinity_data["channel"] = "Affinity";

            var i = 0;
            while(branch_chart_data_source[current_month]["Digital"][i]["branch"] != branch_code ){            
                i++;
            }
            digital_data = branch_chart_data_source[current_month]["Digital"][i];
            digital_data["channel"] = "Digital";

            var i = 0;
            while(branch_chart_data_source[current_month]["Softlogic"][i]["branch"] != branch_code ){            
                i++;
            }
            softlogic_data = branch_chart_data_source[current_month]["Softlogic"][i];
            softlogic_data["channel"] = "Softlogic";

            branch_drilldown_data_array.push(agency_data);
            branch_drilldown_data_array.push(broker_data);
            branch_drilldown_data_array.push(kam_data);
            branch_drilldown_data_array.push(affinity_data);
            branch_drilldown_data_array.push(digital_data);
            branch_drilldown_data_array.push(softlogic_data);

            for (val in branch_drilldown_data_array) {
                branch_drilldown_data_array[val]["target_color"] = "#80daff";
                branch_drilldown_data_array[val]["achieved_color"] = "#0090ff";
                branch_drilldown_data_array[val]["percentage"] = ( (branch_drilldown_data_array[val]["gwp"] *  100)  / branch_drilldown_data_array[val]["target"] ).toFixed(2);
            }
            //console.log(branch_drilldown_data_array);
            //branch_chart_2.dataProvider = branch_2_datasource;
            branch_drilldown_chart.dataProvider = branch_drilldown_data_array;
            branch_drilldown_chart.validateData();    
            
    }
    
    
    // Setup Layout Sizing

	var document_height = $(document).height() - 100;
	var main_chart_height = (document_height * 65) / 100;
	var circle_chart_height = (document_height * 45) / 100;

	$("#main_chart_wrap").height(main_chart_height);
    
	$("#sub_chart_container").height(circle_chart_height);
	$("#channel_chart_wrap").height(circle_chart_height);
	$("#portfolio_chart_wrap").height(circle_chart_height);
    
	$("#branch_chart_1").height(($(document).height() - 80) / 2);
	$("#branch_chart_2").height(($(document).height() - 80) / 2);
	//$("#branch_chart_2").height((document_height - 50) / 2);
    //alert($(document).height() - 70);
	//branch_chart_1
    
    
	// Circle Chart Functions Starts here ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	$('#chart_agency').circliful();
	$('#chart_affinity').circliful();
	$('#chart_kam').circliful();
	$('#chart_broker').circliful();
	$('#chart_digital').circliful();
	$('#chart_softlogic').circliful();
	$('#chart_all').circliful();
	//$('#demo').empty().removeData().attr('data-percent', '60').circliful()

    
	//var height = $("#channel_chart_wrap").height() - 30;
 
    // check whether the display is wide or not
    // if screen isn wide use the height as height variable, else use width as height variable.
    var height = ( $('#agency_chart_wrap').height() > $('#agency_chart_wrap').width() ) ? $('#agency_chart_wrap').width() : $('#agency_chart_wrap').height();
    
    
    height = height - 45;
    
    if($('#agency_chart_wrap').height() > ( $('#agency_chart_wrap').width() + 30 ) ){
        height = height + 30;
        $('#agency_chart_wrap').height( $('#agency_chart_wrap').width() + 30 );
        $('#affinity_chart_wrap').height( $('#agency_chart_wrap').width() + 30 );
        $('#kam_chart_wrap').height( $('#agency_chart_wrap').width() + 30 );
        $('#broker_chart_wrap').height( $('#agency_chart_wrap').width() + 30 );
        $('#digital_chart_wrap').height( $('#digital_chart_wrap').width() + 30 );
        $('#softlogic_chart_wrap').height( $('#softlogic_chart_wrap').width() + 30 );
        //$('#all_chart_wrap').height( $('#agency_chart_wrap').width() + 30 );
        
    }
    
	$('#chart_agency').empty().removeData().attr('data-dimension', height).attr('data-text', '60%').attr('data-percent', 60).circliful();
	$('#chart_affinity').empty().removeData().attr('data-dimension', height).attr('data-text', '60%').attr('data-percent', 60).circliful();
	$('#chart_kam').empty().removeData().attr('data-dimension', height).attr('data-text', '40%').attr('data-percent', 40).circliful();
	$('#chart_broker').empty().removeData().attr('data-dimension', height).attr('data-text', '35%').attr('data-percent', 35).circliful();
	$('#chart_digital').empty().removeData().attr('data-dimension', height).attr('data-text', '35%').attr('data-percent', 35).circliful();
	$('#chart_softlogic').empty().removeData().attr('data-dimension', height).attr('data-text', '35%').attr('data-percent', 35).circliful();
    $('#chart_all').empty().removeData().attr('data-dimension', 270).attr('data-text', '35%').attr('data-percent', 35).circliful();
    

	$("#circle_chart_wrap").click(function () {
		//$('#chart_agency').empty().removeData().attr('data-dimension', height).attr('data-text', '60').circliful()
		//alert();
	});

	
	$('#chart_affinity').click(function () {
		$("#drilldown_title").html("Affinity business breakup " + (current_month == "All" ? "" : "for " + current_month));
		refreshDrillDownCharts("Affinity");
		$("#agency_breakdown_popup").slideDown(100);
	});
	$('#chart_kam').click(function () {
		$("#drilldown_title").html("KAM business breakup " + (current_month == "All" ? "" : "for " + current_month));
		refreshDrillDownCharts("KAM");
		$("#agency_breakdown_popup").slideDown(100);
	});
	$('#chart_agency').click(function () {
		$("#drilldown_title").html("Agency business breakup " + (current_month == "All" ? "" : "for " + current_month));
		refreshDrillDownCharts("Agency");
		$("#agency_breakdown_popup").slideDown(100);
	});
	$('#chart_broker').click(function () {
		$("#drilldown_title").html("Broker business breakup " + (current_month == "All" ? "" : "for " + current_month));
		refreshDrillDownCharts("Broker");
		$("#agency_breakdown_popup").slideDown(100);
	});
	
	// $('#chart_digital').click(function () {
	// 	$("#drilldown_title").html("Digital business breakup " + (current_month == "All" ? "" : "for " + current_month));
	// 	refreshDrillDownCharts("Digital");
	// 	$("#agency_breakdown_popup").slideDown(100); //TODO : change accordingly
	// });
	// $('#chart_softlogic').click(function () {
	// 	$("#drilldown_title").html("Softlogic business breakup " + (current_month == "All" ? "" : "for " + current_month));
	// 	refreshDrillDownCharts("Digital");
	// 	$("#agency_breakdown_popup").slideDown(100); //TODO : change accordingly
	// });

    $('#chart_all').click(function () {
		$("#drilldown_title").html("Total business breakup " + (current_month == "All" ? "" : "for " + current_month));
		refreshDrillDownCharts("All");
		$("#agency_breakdown_popup").slideDown(100);
	});
    
	
	$('#agency_breakdown_popup_close').click(function () {
		$("#agency_breakdown_popup").slideUp(10);
	});

	refreshCircleCharts();
    
    $("#drag_all_chart").click(function(){
        if(total_dragged){
            $( "#all_chart_container" ).animate({
                right: "-300"
            }, 500,function(){
                $("#drag_all_chart").attr('class', 'drag_all_chart');
            });
            total_dragged = false;
        }else{
            $( "#all_chart_container" ).animate({
                right: "+0"
            }, 500,function(){
                $("#drag_all_chart").attr('class', 'drag_all_chart_dragged');
            });
            total_dragged = true;
        }
        
    });

	// Circle Chart Functions Ends here ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    
    
    // 3d cumulated gwp chart Starts here ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    var chartData = [ {
        "category": "Wine left in the barrel",
        "value1": 30,
        "value2": 70
    } ];
   // var chart = AmCharts.makeChart("total_chart_wrap", {
     
/*

var chart = AmCharts.makeChart( "total_chart_wrap", {
  "theme": "light",
  "type": "serial",
  "depth3D": 100,
  "angle": 30,
  "autoMargins": false,
  "marginBottom": 100,
  "marginLeft": 350,
  "marginRight": 300,
  "dataProvider": chartData,
  "valueAxes": [ {
    "stackType": "100%",
    "gridAlpha": 0
  } ],
  "graphs": [ {
    "type": "column",
    "topRadius": 1,
    "columnWidth": 1,
    "showOnAxis": true,
    "lineThickness": 2,
    "lineAlpha": 0.5,
    "lineColor": "#FFFFFF",
    "fillColors": "#8d003b",
    "fillAlphas": 0.8,
    "valueField": "value1"
  }, {
    "type": "column",
    "topRadius": 1,
    "columnWidth": 1,
    "showOnAxis": true,
    "lineThickness": 2,
    "lineAlpha": 0.5,
    "lineColor": "#cdcdcd",
    "fillColors": "#cdcdcd",
    "fillAlphas": 0.5,
    "valueField": "value2"
  } ],

  "categoryField": "category",
  "categoryAxis": {
    "axisAlpha": 0,
    "labelOffset": 40,
    "gridAlpha": 0
  }
} );

*/

	// Charm Bar Functions Starts here ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	// Set Charm Bar All Button Margin Top
	//$("#charm_all_button").css("margin-top", (($(document).height() - 600) / 2) + "px");

	// Black out charm bar -- Hey I'm ready to use for UG People out there
	$(".charm_button_wrap").on('mouseover', function () {
		$("#charm_bar_wrap").css("background-color", "black");
		$(".charm_button_wrap p").css("display", "inherit");
	});

	// Bar Animation Functions Starts Here
	var animating = false;

	// Charm Bar Functions Ends here
	$(document).mousemove(function (e) {
		//$("body").on("mousemove", function(mouse_pointer) {
		//console.log(mouse_pointer.pageX - $(window).scrollLeft());
		//mouse_position = mouse_pointer.pageX - $(window).scrollLeft();
		if (animating) {
			return;
		}
		mouse_position = e.clientX;

		//console.log(mouse_position);
		if (mouse_position <= 100) {
			//SLIDE IN MENU
			animating = true;
			$('#charm_bar_wrap').animate({
				left: 0,
				opacity: 1
			}, 200, function () {
				animating = false;
			});
			//console.log('menu shown');
		} else if (mouse_position > 100) {
			animating = true;
			$('#charm_bar_wrap').animate({
				left: -100,
				opacity: 0
			}, 500, function () {
				animating = false;
			});
			$("#charm_bar_wrap").css("background-color", "");
			$(".charm_button_wrap p").css("display", "none");
			//console.log('menu hidden');
		}
	});

	//gammirissa
    
    $("#charm_toggle_check").change(function(){
        if($(this).is(":checked")) {
            $("#charm_toggle_button_text").html("Channel");            
            $("#channel_chart_wrap").slideDown(300);            
            $("#portfolio_chart_wrap").slideUp(300);
        }else{
            $("#charm_toggle_button_text").html("Portfolio");
            $("#channel_chart_wrap").slideUp(300);
            $("#portfolio_chart_wrap").slideDown(300);
        }
        
    });
    
	$("#charm_all_button").click(function () {
		
		$("#" + "charm_all_button" + " img").attr("src","images/all_icon_selected.png");
		$("#" + "charm_agency_button" + " img").attr("src","images/agency_icon.png");
		$("#" + "charm_affinity_button" + " img").attr("src","images/affinity_icon.png");
		$("#" + "charm_kam_button" + " img").attr("src","images/kam_icon.png");
		$("#" + "charm_broker_button" + " img").attr("src","images/broker_icon.png");
		$("#" + "charm_digital_button" + " img").attr("src","images/digital_icon.png"); //TODO : change the image
		$("#" + "charm_softlogic_button" + " img").attr("src","images/softlogic_icon.png"); //TODO : change the image

		selected_channel = "All";
		refreshMainChart();
		$("#agency_chart_wrap").css("opacity", 1);
		$("#affinity_chart_wrap").css("opacity", 1);
		$("#kam_chart_wrap").css("opacity", 1);
		$("#broker_chart_wrap").css("opacity", 1);
		$("#digital_chart_wrap").css("opacity", 1);
		$("#softlogic_chart_wrap").css("opacity", 1);
        //$("#all_chart_wrap").css("opacity", 1);
	});

	$("#charm_agency_button").click(function () {
		
		$("#" + "charm_all_button" + " img").attr("src","images/all_icon.png");
		$("#" + "charm_agency_button" + " img").attr("src","images/agency_icon_selected.png");
		$("#" + "charm_affinity_button" + " img").attr("src","images/affinity_icon.png");
		$("#" + "charm_kam_button" + " img").attr("src","images/kam_icon.png");
		$("#" + "charm_broker_button" + " img").attr("src","images/broker_icon.png");
		$("#" + "charm_digital_button" + " img").attr("src","images/digital_icon.png"); //TODO : change the image
		$("#" + "charm_softlogic_button" + " img").attr("src","images/softlogic_icon.png"); //TODO : change the image

		selected_channel = "Agency";
		refreshMainChart();
		$("#agency_chart_wrap").css("opacity", 1);
		$("#affinity_chart_wrap").css("opacity", 0.2);
		$("#kam_chart_wrap").css("opacity", 0.2);
		$("#broker_chart_wrap").css("opacity", 0.2);
		$("#softlogic_chart_wrap").css("opacity", 0.2);
		$("#digital_chart_wrap").css("opacity", 0.2);
        //$("#all_chart_wrap").css("opacity", 0.2);
	});

	$("#charm_affinity_button").click(function () {
		
		$("#" + "charm_all_button" + " img").attr("src","images/all_icon.png");
		$("#" + "charm_agency_button" + " img").attr("src","images/agency_icon.png");
		$("#" + "charm_affinity_button" + " img").attr("src","images/affinity_icon_selected.png");
		$("#" + "charm_kam_button" + " img").attr("src","images/kam_icon.png");
		$("#" + "charm_broker_button" + " img").attr("src","images/broker_icon.png");
		$("#" + "charm_digital_button" + " img").attr("src","images/digital_icon.png"); //TODO : change the image
		$("#" + "charm_softlogic_button" + " img").attr("src","images/softlogic_icon.png"); //TODO : change the image

		selected_channel = "Affinity";
		refreshMainChart();
		$("#agency_chart_wrap").css("opacity", 0.2);
		$("#affinity_chart_wrap").css("opacity", 1);
		$("#kam_chart_wrap").css("opacity", 0.2);
		$("#broker_chart_wrap").css("opacity", 0.2);
		$("#softlogic_chart_wrap").css("opacity", 0.2);
		$("#digital_chart_wrap").css("opacity", 0.2);
        //$("#all_chart_wrap").css("opacity", 0.2);
	});

	$("#charm_kam_button").click(function () {
		
		$("#" + "charm_all_button" + " img").attr("src","images/all_icon.png");
		$("#" + "charm_agency_button" + " img").attr("src","images/agency_icon.png");
		$("#" + "charm_affinity_button" + " img").attr("src","images/affinity_icon.png");
		$("#" + "charm_kam_button" + " img").attr("src","images/kam_icon_selected.png");
		$("#" + "charm_broker_button" + " img").attr("src","images/broker_icon.png");
		$("#" + "charm_digital_button" + " img").attr("src","images/digital_icon.png"); //TODO : change the image
		$("#" + "charm_softlogic_button" + " img").attr("src","images/softlogic_icon.png"); //TODO : change the image


		selected_channel = "KAM";
		refreshMainChart();
		$("#agency_chart_wrap").css("opacity", 0.2);
		$("#affinity_chart_wrap").css("opacity", 0.2);
		$("#kam_chart_wrap").css("opacity", 1);
		$("#broker_chart_wrap").css("opacity", 0.2);
		$("#softlogic_chart_wrap").css("opacity", 0.2);
		$("#digital_chart_wrap").css("opacity", 0.2);
        //$("#all_chart_wrap").css("opacity", 0.2);
	});

	$("#charm_broker_button").click(function () {
		
		$("#" + "charm_all_button" + " img").attr("src","images/all_icon.png");
		$("#" + "charm_agency_button" + " img").attr("src","images/agency_icon.png");
		$("#" + "charm_affinity_button" + " img").attr("src","images/affinity_icon.png");
		$("#" + "charm_kam_button" + " img").attr("src","images/kam_icon.png");
		$("#" + "charm_broker_button" + " img").attr("src","images/broker_icon_selected.png");
		$("#" + "charm_digital_button" + " img").attr("src","images/digital_icon.png"); //TODO : change the image
		$("#" + "charm_softlogic_button" + " img").attr("src","images/softlogic_icon.png"); //TODO : change the image

		selected_channel = "Broker";
		refreshMainChart();
		$("#agency_chart_wrap").css("opacity", 0.2);
		$("#affinity_chart_wrap").css("opacity", 0.2);
		$("#kam_chart_wrap").css("opacity", 0.2);
		$("#broker_chart_wrap").css("opacity", 1);
		$("#softlogic_chart_wrap").css("opacity", 0.2);
		$("#digital_chart_wrap").css("opacity", 0.2);
        //$("#all_chart_wrap").css("opacity", 0.2);
	});

	$("#charm_digital_button").click(function () {
		
		$("#" + "charm_all_button" + " img").attr("src","images/all_icon.png");
		$("#" + "charm_agency_button" + " img").attr("src","images/agency_icon.png");
		$("#" + "charm_affinity_button" + " img").attr("src","images/affinity_icon.png");
		$("#" + "charm_kam_button" + " img").attr("src","images/kam_icon.png");
		$("#" + "charm_broker_button" + " img").attr("src","images/broker_icon.png");
		$("#" + "charm_digital_button" + " img").attr("src","images/digital_icon_selected.png"); //TODO : change the image
		$("#" + "charm_softlogic_button" + " img").attr("src","images/softlogic_icon.png"); //TODO : change the image


		selected_channel = "Digital";
		refreshMainChart();
		$("#agency_chart_wrap").css("opacity", 0.2);
		$("#affinity_chart_wrap").css("opacity", 0.2);
		$("#kam_chart_wrap").css("opacity", 0.2);
		$("#broker_chart_wrap").css("opacity", 0.2);
		$("#softlogic_chart_wrap").css("opacity", 0.2);
		$("#digital_chart_wrap").css("opacity", 1);
        //$("#all_chart_wrap").css("opacity", 0.2);
	});

	$("#charm_softlogic_button").click(function () {
		
		$("#" + "charm_all_button" + " img").attr("src","images/all_icon.png");
		$("#" + "charm_agency_button" + " img").attr("src","images/agency_icon.png");
		$("#" + "charm_affinity_button" + " img").attr("src","images/affinity_icon.png");
		$("#" + "charm_kam_button" + " img").attr("src","images/kam_icon.png");
		$("#" + "charm_broker_button" + " img").attr("src","images/broker_icon.png");
		$("#" + "charm_digital_button" + " img").attr("src","images/digital_icon.png"); //TODO : change the image
		$("#" + "charm_softlogic_button" + " img").attr("src","images/softlogic_icon_selected.png"); //TODO : change the image


		selected_channel = "Softlogic";
		refreshMainChart();
		$("#agency_chart_wrap").css("opacity", 0.2);
		$("#affinity_chart_wrap").css("opacity", 0.2);
		$("#kam_chart_wrap").css("opacity", 0.2);
		$("#broker_chart_wrap").css("opacity", 0.2);
		$("#digital_chart_wrap").css("opacity", 0.2);
		$("#softlogic_chart_wrap").css("opacity", 1);
        //$("#all_chart_wrap").css("opacity", 0.2);
	});

	// Charm Bar Functions Ends here ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	// Main Chart Functions Starts here -----------------------------------------------------------------------------------
	var main_chart = AmCharts.makeChart("main_chart_wrap", {
		"theme": "light",
		"type": "serial",
		color: "#eee",
		"dataProvider": [],
		"valueAxes": [{
			"unit": "/=",
			"position": "left",
			"title": "GWP",
			"minimum": 0,
			"gridAlpha": 0,
                }, {
			"id": "a1",
			"unit": "/=",
			"position": "right",
			"title": "Cumulated GWP",
			"minimum": 0,
			"gridAlpha": 0,
                }],

		"startDuration": 1,
		"graphs": [{
			"balloonText": "Target GWP in [[category]] : <b>[[value]] <br/> (Achieved : [[percentage]]%) </b>",
			"fillAlphas": 0.9,
			"lineAlpha": 0.2,
			"title": "2004",
			"type": "column",
			"valueField": "target",
			"colorField": "target_color"
                }, {
			"balloonText": "Achieved GWP in [[category]] : <b>[[value]] <br/> (Achieved : [[percentage]]%) </b>",
			"fillAlphas": 0.9,
			"lineAlpha": 0.2,
			"title": "2005",
			"type": "column",
			"clustered": false,
			"columnWidth": 0.7,
			"valueField": "achieved",
			"colorField": "achieved_color"
                }, {
			id: "g2",
			valueField: "cumulated_target",
			classNameField: "bulletClass",
			title: "Target",
			type: "line",
			valueAxis: "a1",
			lineColor: "#000000",
			lineThickness: 1,
			lineAlpha: 0.3,
			legendValueText: "[[description]]/[[value]]",
			descriptionField: "townName",
			bullet: "triangleDown",
			bulletSize: 10,
			bulletSizeField: "townSize",
			bulletBorderColor: "#ff6600",
			bulletBorderAlpha: 1,
			bulletBorderThickness: 2,
			bulletColor: "#ffcfaf",
			labelText: "[[townName2]]",
			labelPosition: "right",
			balloonText: " Target :[[value]]",
			showBalloon: true,
			animationPlayed: true,
              }, , {
			id: "g3",
			valueField: "cumulated_achieved",
			classNameField: "bulletClass",
			title: "Achievement",
			type: "line",
			valueAxis: "a1",
			lineColor: "#000000",
			lineAlpha: 0.3,
			lineThickness: 1,
			legendValueText: "[[description]]/[[value]]",
			descriptionField: "townName",
			bullet: "round",
			bulletSize: 10,
			bulletSizeField: "townSize",
			bulletBorderColor: "#ff6600",
			bulletBorderAlpha: 1,
			bulletBorderThickness: 2,
			bulletColor: "#ffcfaf",
			labelText: "[[townName2]]",
			labelPosition: "right",
			balloonText: " Achieved :[[value]]",
			showBalloon: true,
			animationPlayed: true,
              }],
		"plotAreaFillAlphas": 0.1,
		"categoryField": "month",
		"categoryAxis": {
			"gridPosition": "start",
			"dashLength": 6,
			"gridPosition": "middle",
			"gridColor": "red"
		},
		"export": {
			"enabled": true
		}

	}); // End of main chart
    
	refreshMainChart();
    
    main_chart.addListener("clickGraphItem", handleMainChartClick);

	function handleMainChartClick(event) {

		if (current_month == event.item.category) {
			// Exit user to all month	
			main_chart.dataProvider[getMonthNumberFromMonth(current_month)].target_color = "#80daff";
			main_chart.dataProvider[getMonthNumberFromMonth(current_month)].achieved_color = "#0090ff";
			current_month = "All";
		} else {
			if (current_month != "All") {
				main_chart.dataProvider[getMonthNumberFromMonth(current_month)].target_color = "#80daff";
				main_chart.dataProvider[getMonthNumberFromMonth(current_month)].achieved_color = "#0090ff";
			}
			main_chart.dataProvider[getMonthNumberFromMonth(event.item.category)].target_color = "#c3bb39";
			main_chart.dataProvider[getMonthNumberFromMonth(event.item.category)].achieved_color = "#fff000";

			current_month = event.item.category;
		}



		main_chart.validateData();

		refreshCircleCharts();
        refreshPortfolioChart();
	}
    
    // Main Chart Functions Ends here -----------------------------------------------------------------------------------
    
    // Portfolio Chart Functions Starts here -----------------------------------------------------------------------------------
	var portfolio_chart = AmCharts.makeChart("chart_portfolio", {
		"theme": "light",
		"type": "serial",
		color: "#eee",
		"dataProvider": [{
            month: "January",
            portfolio: "Motor",
            target: "100000",
            gwp: "215286570.76"
        }],
		"valueAxes": [{
			"unit": "/=",
			"position": "left",
			"title": "GWP",
			"minimum": 0,
			"gridAlpha": 0,
                }],

		"startDuration": 1,
		"graphs": [{
			"balloonText": "Target GWP in [[category]] : <b>[[value]] <br/> (Achieved : [[percentage]]%) </b>",
			"fillAlphas": 0.9,
			"lineAlpha": 0.2,
			"title": "2004",
			"type": "column",
			"valueField": "target",
			"colorField": "target_color"
                }, {
			"balloonText": "Achieved GWP in [[category]] : <b>[[value]] <br/> (Achieved : [[percentage]]%) </b>",
			"fillAlphas": 0.9,
			"lineAlpha": 0.2,
			"title": "2005",
			"type": "column",
			"clustered": false,
			"columnWidth": 0.7,
			"valueField": "achieved",
			"colorField": "achieved_color"
                }],
		"plotAreaFillAlphas": 0.1,
		"categoryField": "portfolio",
		"categoryAxis": {
			"gridPosition": "start",
			"dashLength": 6,
			"gridPosition": "middle",
			"gridColor": "red"
		},
		"export": {
			"enabled": true
		}

	}); // End of portfolio chart
    refreshPortfolioChart();
    //	refreshMainChart();
    
    //    main_chart.addListener("clickGraphItem", handleClick);

	/*function handleClick(event) {

		if (current_month == event.item.category) {
			// Exit user to all month	
			main_chart.dataProvider[getMonthNumberFromMonth(current_month)].target_color = "#80daff";
			main_chart.dataProvider[getMonthNumberFromMonth(current_month)].achieved_color = "#0090ff";
			current_month = "All";
		} else {
			if (current_month != "All") {
				main_chart.dataProvider[getMonthNumberFromMonth(current_month)].target_color = "#80daff";
				main_chart.dataProvider[getMonthNumberFromMonth(current_month)].achieved_color = "#0090ff";
			}
			main_chart.dataProvider[getMonthNumberFromMonth(event.item.category)].target_color = "#c3bb39";
			main_chart.dataProvider[getMonthNumberFromMonth(event.item.category)].achieved_color = "#fff000";

			current_month = event.item.category;
		}



		main_chart.validateData();

		refreshCircleCharts();
	}*/
    
    // Portfolio Chart Functions Ends here -----------------------------------------------------------------------------------
    
    // Drill Down Chart Functions Starts here ---------------------------------------------------------------------------

	var drilldown_chart_motor_nonmotor = AmCharts.makeChart("motor_nonmotor", {
		"type": "pie",
		"theme": "none",
		"labelRadius": -35,
		"labelText": "[[category]] \n [[gwp]] \n [[percents]]%",
		"dataProvider": [
			{
				"category": "Motor",
				"gwp": 501.9,
				"color": "#FF6600"
    		},
			{
				"category": "Non Motor",
				"gwp": 301.9,
				"color": "#FF0F00"
    		}],
		"colorField": "color",
		"valueField": "gwp",
		"titleField": "category",
		"labelsEnabled": true,
		"autoMargins": false,
		"marginTop": 10,
		"marginBottom": 10,
		"marginLeft": 10,
		"marginRight": 10,
		"pullOutRadius": 10,
		"exportConfig": {
			"menuItems": [{
				"icon": '/lib/3/images/export.png',
				"format": 'png'
      }]
		}
	});

	var drilldown_chart_newbusiness_renewal = AmCharts.makeChart("newbusiness_renewal", {
		"type": "pie",
		"theme": "none",
		"labelRadius": -35,
		"labelText": "[[category]] \n [[gwp]] \n [[percents]]%",
		"dataProvider": [
			{
				"category": "New Business",
				"gwp": 0,
				"color": "#FF6600"
    		},
			{
				"category": "Renewal",
				"gwp": 301.9,
				"color": "#FF0F00"
    		}],
		"colorField": "color",
		"valueField": "gwp",
		"titleField": "category",
		"labelsEnabled": true,
		"autoMargins": false,
		"marginTop": 10,
		"marginBottom": 10,
		"marginLeft": 10,
		"marginRight": 10,
		"pullOutRadius": 10,
		"exportConfig": {
			"menuItems": [{
				"icon": '/lib/3/images/export.png',
				"format": 'png'
      }]
		}
	});

    // Drill Down Chart Functions Ends here ---------------------------------------------------------------------------
    
    // Branch Chart Functions Starts here -----------------------------------------------------------------------------
	
	var branch_chart_1 = AmCharts.makeChart("branch_chart_1", {
		"theme": "light",
		"type": "serial",
		"dataProvider": [],
		"valueAxes": [{
			"unit": "/=",
			"position": "left",
			"title": "GWP",
			"minimum": 0,
			"gridAlpha": 0,
                }],

		"startDuration": 1,
		"graphs": [
			{
				"balloonText": "Target GWP in [[category]] : <b>[[value]] <br/> (Achieved : [[percentage]]%) </b>",
				"fillAlphas": 0.9,
				"lineAlpha": 0.2,
				"title": "Target",
				"type": "column",
				"valueField": "target",
				"colorField": "target_color"
			},
			{
				"balloonText": "Achieved GWP in [[category]] : <b>[[value]] <br/> (Achieved : [[percentage]]%) </b>",
				"fillAlphas": 0.9,
				"lineAlpha": 0.2,
				"title": "GWP",
				"type": "column",
				"clustered": false,
				"columnWidth": 0.5,
				"valueField": "gwp",
				"colorField": "achieved_color"
                }],
		"plotAreaFillAlphas": 0.1,
		"categoryField": "branch",
		"categoryAxis": {
			"gridPosition": "start",
			"dashLength": 6,
			"gridPosition": "middle",
			"gridColor": "red",
			"autoGridCount":false
		},
		"export": {
			"enabled": true
		}

	}); // End of branch chart 1
    
    branch_chart_1.addListener("clickGraphItem", handleBranchChartClick);

	function handleBranchChartClick(event) {
        if (selected_channel_for_branch == "All"){
        //if(selected_channel == "All"){
            var title_text = "Drilldown of " + event.item.category;
            if(current_month != "All"){
                title_text += " for " + current_month;
            }
            $("#branch_drilldown_title").html(title_text);
            refreshBranchDrillDownChart(event.item.category);
            $("#branch_breakdown_popup").slideDown(100);    
        }
        
        //alert(event.item.category);
    }
    
    $('#branch_breakdown_popup_close').click(function () {
		$("#branch_breakdown_popup").slideUp(10);
	});
    
	var branch_chart_2 = AmCharts.makeChart("branch_chart_2", {
		"theme": "light",
		"type": "serial",
		"dataProvider": [],
		"valueAxes": [{
			"unit": "/=",
			"position": "left",
			"title": "GWP",
			"minimum": 0,
			"gridAlpha": 0,
                }],

		"startDuration": 1,
		"graphs": [
			{
				"balloonText": "Target GWP in [[category]] : <b>[[value]] <br/> (Achieved : [[percentage]]%) </b>",
                "labelText": "",
                "labelPosition": "bottom",
				"fillAlphas": 0.9,
				"lineAlpha": 0.2,
				"title": "Target",
				"type": "column",
				"valueField": "target",
				"colorField": "target_color"
			 },
			{
				"balloonText": "Achieved GWP in [[category]] : <b>[[value]] <br/> (Achieved : [[percentage]]%) </b>",
				"fillAlphas": 0.9,
				"lineAlpha": 0.2,
				"title": "GWP",
				"type": "column",
				"clustered": false,
				"columnWidth": 0.5,
				"valueField": "gwp",
				"colorField": "achieved_color"
                }],
		"plotAreaFillAlphas": 0.1,
		"categoryField": "branch",
		"categoryAxis": {
			"gridPosition": "start",
			"dashLength": 6,
			"gridPosition": "middle",
			"gridColor": "red",
			"autoGridCount":false,
		},
		"export": {
			"enabled": true
		}

	}); // End of branch chart 2

    branch_chart_2.addListener("clickGraphItem", handleBranchChartClick);
    
    // Branch Drill Down Chart Functions Starts here -----------------------------------------------------------------------------------
	var branch_drilldown_chart = AmCharts.makeChart("branch_breakdown_chart", {
		"theme": "light",
		"type": "serial",
		color: "#eee",
		"dataProvider": [{
            month: "January",
            portfolio: "Motor",
            target: "100000",
            gwp: "215286570.76"
        }],
		"valueAxes": [{
			"unit": "/=",
			"position": "left",
			"title": "GWP",
			"minimum": 0,
			"gridAlpha": 0,
                }],

		"startDuration": 1,
		"graphs": [{
			"balloonText": "Target GWP in [[category]] : <b>[[value]] <br/> (Achieved : [[percentage]]%) </b>",
			"fillAlphas": 0.9,
			"lineAlpha": 0.2,
			"title": "2004",
			"type": "column",
			"valueField": "target",
			"colorField": "target_color"
                }, {
			"balloonText": "Achieved GWP in [[category]] : <b>[[value]] <br/> (Achieved : [[percentage]]%) </b>",
			"fillAlphas": 0.9,
			"lineAlpha": 0.2,
			"title": "2005",
			"type": "column",
			"clustered": false,
			"columnWidth": 0.7,
			"valueField": "gwp",
			"colorField": "achieved_color"
                }],
		"plotAreaFillAlphas": 0.1,
		"categoryField": "channel",
		"categoryAxis": {
			"gridPosition": "start",
			"dashLength": 6,
			"gridPosition": "middle",
			"gridColor": "red"
		},
		"export": {
			"enabled": true
		}

	}); // End of branch_drilldown chart
    
   
	 $("#branch_popup_agency").on('click', function () {
		
		refreshBranchCharts("Agency");
		
		if(current_month == "All"){
			$('#branch_chart_title').html("Branch wise " + "Agency" + " achievements");
		}else{
			$('#branch_chart_title').html("Branch wise " + "Agency" + " achievements for " +  current_month );
		}
		$('#modal-fullscreen').modal('show');
	});
	
	$("#branch_popup_affinity").on('click', function () {
		
		refreshBranchCharts("Affinity");
		
		if(current_month == "All"){
			$('#branch_chart_title').html("Branch wise " + "Affinity" + " achievements");
		}else{
			$('#branch_chart_title').html("Branch wise " + "Affinity" + " achievements for " +  current_month );
		}
		$('#modal-fullscreen').modal('show');
	});
	
	$("#branch_popup_broker").on('click', function () {
		
		refreshBranchCharts("Broker");
		
		if(current_month == "All"){
			$('#branch_chart_title').html("Branch wise " + "Broker" + " achievements");
		}else{
			$('#branch_chart_title').html("Branch wise " + "Broker" + " achievements for " +  current_month );
		}
		$('#modal-fullscreen').modal('show');
	});
	
	$("#branch_popup_kam").on('click', function () {
		
		refreshBranchCharts("KAM");
		if(current_month == "All"){
			$('#branch_chart_title').html("Branch wise " + "KAM" + " achievements");
		}else{
			$('#branch_chart_title').html("Branch wise " + "KAM" + " achievements for " +  current_month );
		}
		
		$('#modal-fullscreen').modal('show');
	});
    

    $("#branch_popup_all").on('click', function () {
		
		refreshBranchCharts("All");
		if(current_month == "All"){
			$('#branch_chart_title').html("Branch wise " + "Total" + " achievements");
		}else{
			$('#branch_chart_title').html("Branch wise " + "Total" + " achievements for " +  current_month );
		}
		
		$('#modal-fullscreen').modal('show');
	});
      
    // Branch Chart Functions Ends here -----------------------------------------------------------------------------
    

	$(".modal-transparent").on('show.bs.modal', function () {
		setTimeout(function () {
			$(".modal-backdrop").addClass("modal-backdrop-transparent");
		}, 0);
	});
	$(".modal-transparent").on('hidden.bs.modal', function () {
		$(".modal-backdrop").addClass("modal-backdrop-transparent");
	});

	$(".modal-fullscreen").on('show.bs.modal', function () {
		setTimeout(function () {
			$(".modal-backdrop").addClass("modal-backdrop-fullscreen");
		}, 0);
	});
	$(".modal-fullscreen").on('hidden.bs.modal', function () {
		$(".modal-backdrop").addClass("modal-backdrop-fullscreen");
	});


	
	





}); // End of document.ready


function reloadData() {
	jQuery.ajax({

		url: datasource_url + 'main_chart.php',
		success: function (result) {
			json_result = JSON.parse(result)
			main_chart_data_source = json_result;

			//console.log(result);
		},
		async: false
	});

	jQuery.ajax({

		url: datasource_url + 'circle_chart.php',
		success: function (result) {
			json_result = JSON.parse(result)
			circle_chart_data_source = json_result;

			//console.log(result);
		},
		async: false
	});
	jQuery.ajax({

		url: datasource_url + 'drilldown_chart.php',
		success: function (result) {
			json_result = JSON.parse(result)
			drilldown_chart_data_source = json_result;

			//console.log(result);
		},
		async: false
	});
	jQuery.ajax({

		url: datasource_url + 'branch_chart.php',
		success: function (result) {
			json_result = JSON.parse(result)
			branch_chart_data_source = json_result;

			//console.log(result);
		},
		async: false
	});
    jQuery.ajax({

		url: datasource_url + 'portfolio_chart.php',
		success: function (result) {
			json_result = JSON.parse(result)
			portfolio_chart_data_source = json_result;

			//console.log(result);
		},
		async: false
	});

    
    // finally hide loading screen
    $( "#loading_screen" ).fadeOut( "slow" )

}

function getMonthNumberFromMonth(month) {
	var month_number;
	switch (month) {
	case "January":
		month_number = 0;
		break;
	case "February":
		month_number = 1;
		break;
	case "March":
		month_number = 2;
		break;
	case "April":
		month_number = 3;
		break;
	case "May":
		month_number = 4;
		break;
	case "June":
		month_number = 5;
		break;
	case "July":
		month_number = 6;
		break;
	case "August":
		month_number = 7;
		break;
	case "September":
		month_number = 8;
		break;
	case "October":
		month_number = 9;
		break;
	case "November":
		month_number = 10;
		break;
	case "December":
		month_number = 11;
		break;
	}
	return month_number;
}
