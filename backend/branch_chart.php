<?php
header("Access-Control-Allow-Origin: *");
// $servername = "172.24.10.45:3307";
// $username = "root";
// $password = "ace101";
// $dbname = "gwp_dashboard_aig";

$servername = "172.24.10.48";
$username = "gwp_user";
$password = "Gwp@123!";
$dbname = "gwp_dashboard_live";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$rows = array();

$sql = "
SELECT branch_targets.*, A.gwp FROM branch_targets 

LEFT JOIN 
(
SELECT 
CASE SUBSTR(TRDTGW,5,2)
WHEN '01' THEN 'January' 
WHEN '02' THEN 'February' 
WHEN '03' THEN 'March' 
WHEN '04' THEN 'April' 
WHEN '05' THEN 'May' 
WHEN '06' THEN 'June' 
WHEN '07' THEN 'July' 
WHEN '08' THEN 'August' 
WHEN '09' THEN 'September' 
WHEN '10' THEN 'October' 
WHEN '11' THEN 'November' 
WHEN '12' THEN 'December' 
ELSE 'N/A' 
END AS `month`, 
CASE PLCHGW 
WHEN 'AFN' THEN 'Affinity' 
WHEN 'AGT' THEN 'Agency' 
WHEN 'BRK' THEN 'Broker' 
WHEN 'KAM' THEN 'KAM' 
WHEN 'SFL' THEN 'Softlogic' 
WHEN 'DGT' THEN 'Digital'
WHEN 'INS' THEN 'Institutional'
ELSE 'N/A'
END as channel,  
BRCDGW as branch, 
ROUND(SUM(GWPAGW),2) as gwp 
FROM gwp_source 
GROUP BY SUBSTR(TRDTGW,5,2),PLCHGW, BRCDGW
) AS A 
ON branch_targets.`month` = A.`month` AND branch_targets.channel = A.channel AND branch_targets.branch = A.branch 
ORDER BY FIELD(branch_targets.`month`,'January','February','March','April','May','June','July','August','September','October','November','December')
, branch_targets.branch, branch_targets.channel 
;

";



$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
	
	
    while($row = $result->fetch_assoc()) {
        
		$rows[$row["month"]][$row["channel"]][]= array("branch" => $row["branch"], "target" => $row["target"], "gwp"=>$row["gwp"], );
		
		
		
//		if( isset($rows["All"][$row["channel"]][$row["branch"]]) ){
//			$rows["All"][$row["channel"]][] = array("Branch" => $row["branch"], "gwp"=>$row["gwp"]);
//			//$rows["All"][$row["channel"]][$row["branch"]] += $row["gwp"];
//		}else{
//			$rows["All"][$row["channel"]][] = array("Branch" => $row["branch"], "gwp"=>$row["gwp"]);
//			//$rows["All"][$row["channel"]][$row["branch"]] = $row["gwp"];
//		}
    }
	
} 


//print('<pre>'); print_r($rows); exit();

/*
foreach ($rows as $key => $value){
    $all_rows = [];
    
    ////echo $key;//Prints Month
    ////echo "</br></br>";
    $iterator = 0;
    foreach ($rows[$key] as $key_1 => $value_1){
        //echo $key_1;
        //echo "</br></br>";
        ////echo "</br></br>";
        ////echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ".$iterator;
        ////echo "</br></br>";
        for($i = 0; $i < sizeOf($value_1); $i++){
//            print_r($value_1[$i]["gwp"]);
//            die();
            //echo "</br>";                
            if($iterator == 0){
                $all_rows = array("All"=>$value_1);    
            }else{
                $all_rows["All"][$i]["gwp"] = $all_rows["All"][$i]["gwp"] + $value_1[$i]["gwp"];
                $all_rows["All"][$i]["target"] = $all_rows["All"][$i]["gwp"] + $value_1[$i]["target"];
            }
            
            
        }   
        //echo "<br/><br/>~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<br/><br/>";
        $iterator++;
        
    }
        $rows[$key]["All"] = $all_rows["All"];
        ////print json_encode($all_rows);
    
   //// echo "<br/><br/>~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<br/><br/>";
}
////die();
/*
*/

// This part adds sum ups of all months
/*
$sql = "
SELECT branch_targets.*, A.gwp FROM branch_targets 

LEFT JOIN 
(
SELECT 
CASE SUBSTR(TRDTGW,5,2)
WHEN '01' THEN 'January' 
WHEN '02' THEN 'February' 
WHEN '03' THEN 'March' 
WHEN '04' THEN 'April' 
WHEN '05' THEN 'May' 
WHEN '06' THEN 'June' 
WHEN '07' THEN 'July' 
WHEN '08' THEN 'August' 
WHEN '09' THEN 'September' 
WHEN '10' THEN 'October' 
WHEN '11' THEN 'November' 
WHEN '12' THEN 'December' 
ELSE 'N/A' 
END AS `month`, 
CASE PLCHGW 
WHEN 'AFN' THEN 'Affinity' 
WHEN 'AGT' THEN 'Agency' 
WHEN 'BRK' THEN 'Broker' 
WHEN 'KAM' THEN 'KAM' 
ELSE 'N/A'
END as channel,  
BRCDGW as branch, 
ROUND(SUM(GWPAGW),2) as gwp 
FROM gwp_source 
GROUP BY SUBSTR(TRDTGW,5,2),PLCHGW, BRCDGW
) AS A 
ON branch_targets.`month` = A.`month` AND branch_targets.channel = A.channel AND branch_targets.branch = A.branch

GROUP BY  branch_targets.channel, branch_targets.branch

;

";
*/

$sql = "


SELECT B.*, A.gwp FROM 
(
	SELECT channel, branch, SUM(target) as target FROM branch_targets GROUP BY channel, branch 
)
AS B 
LEFT JOIN 
(
SELECT 
CASE PLCHGW 
WHEN 'AFN' THEN 'Affinity' 
WHEN 'AGT' THEN 'Agency' 
WHEN 'BRK' THEN 'Broker' 
WHEN 'KAM' THEN 'KAM' 
WHEN 'SFL' THEN 'Softlogic' 
WHEN 'DGT' THEN 'Digital'
WHEN 'INS' THEN 'Institutional'
ELSE 'N/A'
END as channel,  
BRCDGW as branch, 
ROUND(SUM(GWPAGW),2) as gwp 
FROM gwp_source 
GROUP BY PLCHGW, BRCDGW
) AS A 
ON B.channel = A.channel AND B.branch = A.branch
GROUP BY  B.channel, B.branch
ORDER BY B.branch, channel;

";



$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
	
	
    while($row = $result->fetch_assoc()) {
        

		$rows["All"][$row["channel"]][]= array("branch" => $row["branch"], "target" => $row["target"], "gwp"=>$row["gwp"], );
		
		
		
//		if( isset($rows["All"][$row["channel"]][$row["branch"]]) ){
//			$rows["All"][$row["channel"]][] = array("Branch" => $row["branch"], "gwp"=>$row["gwp"]);
//			//$rows["All"][$row["channel"]][$row["branch"]] += $row["gwp"];
//		}else{
//			$rows["All"][$row["channel"]][] = array("Branch" => $row["branch"], "gwp"=>$row["gwp"]);
//			//$rows["All"][$row["channel"]][$row["branch"]] = $row["gwp"];
//		}
    }
	
} 


foreach ($rows as $key => $value){
    $all_rows = [];
    $iterator = 0;
    foreach ($rows[$key] as $key_1 => $value_1){
        
        for($i = 0; $i < sizeOf($value_1); $i++){              
            if($iterator == 0){
                $all_rows = array("All"=>$value_1);    
            }else{
                $all_rows["All"][$i]["gwp"] = $all_rows["All"][$i]["gwp"] + $value_1[$i]["gwp"];


                    $targetVal1 = $all_rows["All"][$i]["target"];
                    $targetVal2 = $value_1[$i]["target"];


                $all_rows["All"][$i]["target"] = $targetVal1 + $targetVal2;
            }
        }   
        $iterator++;
    }
        $rows[$key]["All"] = $all_rows["All"];
}

print json_encode($rows);
$conn->close();
?>
