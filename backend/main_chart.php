<?php
header("Access-Control-Allow-Origin: *");
// $servername = "172.24.10.45:3307";
// $username = "root";
// $password = "ace101";
// $dbname = "gwp_dashboard_aig";

$servername = "172.24.10.48";
$username = "gwp_user";
$password = "Gwp@123!";
$dbname = "gwp_dashboard_live";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "

SELECT A.`month`, A.channel, A.target, A.rv_target, SUM(B.achieved) AS achieved FROM 
(
	SELECT `month`,channel, target as target, rv_target as rv_target FROM targets_with_revised
) AS A 
LEFT JOIN 
(
		SELECT 
		CASE SUBSTR(TRDTGW,5,2) 
		WHEN '01' THEN 'January' 
		WHEN '02' THEN 'February' 
		WHEN '03' THEN 'March' 
		WHEN '04' THEN 'April' 
		WHEN '05' THEN 'May' 
		WHEN '06' THEN 'June' 
		WHEN '07' THEN 'July' 
		WHEN '08' THEN 'August' 
		WHEN '09' THEN 'September' 
		WHEN '10' THEN 'October' 
		WHEN '11' THEN 'November' 
		WHEN '12' THEN 'December' 
		ELSE 'N/A' 
		END as month, 

		CASE PLCHGW 
		WHEN 'AFN' THEN 'Affinity' 
		WHEN 'AGT' THEN 'Agency' 
		WHEN 'BRK' THEN 'Broker' 
		WHEN 'KAM' THEN 'KAM' 
		WHEN 'SFL' THEN 'Softlogic' 
		WHEN 'DGT' THEN 'Digital'
		WHEN 'BNK' THEN 'BNK'
		ELSE 'N/A'
		END as channel

		, ROUND(SUM(GWPAGW),2) as achieved 
		FROM gwp_source 

		GROUP BY SUBSTR(TRDTGW,5,2), PLCHGW 

) as B
ON B.`month` = A.`month`
AND B.channel = A.channel
GROUP BY `month`, channel

";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
	//$rows = array();
	
	$cum_target_kam = 0;
	$cum_rv_target_kam = 0;	
	$cum_achieved_kam = 0;
	
	$cum_target_agency = 0;
	$cum_rv_target_agency = 0;
	$cum_achieved_agency = 0;
	
	$cum_target_broker = 0;
	$cum_rv_target_broker = 0;
	$cum_achieved_broker = 0;
	
	$cum_target_affinity = 0;
	$cum_rv_target_affinity = 0;
	$cum_achieved_affinity = 0;


	$revised_target 		= 0;

	
	$cum_target_digital = 0;
	$cum_rv_target_digital = 0;
	$cum_achieved_digital = 0;
	
	$cum_target_softlogic = 0;
	$cum_rv_target_softlogic = 0;
	$cum_achieved_softlogic = 0;

	$cum_target_banca =0;
	$cum_rv_target_banca =0;
	$cum_achieved_banca = 0;
	
	$total_month = 0;
	$total_target = 0;
	$total_achieved = 0;
	$cum_total_target = 0;
	$cum_total_rv_target = 0;
	$cum_total_achieved = 0;
	$total_rv_target = 0;
	$rev_target =0;


	$kam = array();
	$agency = array();
	$broker = array();
	$affinity = array();
	$digital = array();
	$softlogic = array();
	$bnk = array();

	$all = array();
	
	$i = 0;
    while($row = $result->fetch_assoc()) {

	//print('<pre>');print_r($row); exit;
    	if(!$row["target"]){
    	//	$row["target"] = $row["achieved"];
    	}

		if($i <= 6){

			//echo '<br/>'.$i.'-'.$row["channel"]; 
			$total_target 				+= $row["target"];
			$total_achieved 			+= $row["achieved"];
			$cum_total_target 			+= $row["target"];
			$total_rv_target 			+= $row["rv_target"];
			// $cum_total_revised_target 	+=  $row["rv_target"];

			$cum_total_achieved 		+= $row["achieved"];
			$total_month  				= $row["month"];

			$i++;
		}else{



			$cum_achieved_current = null;
			if($total_achieved != 0){
				$cum_achieved_current = $cum_total_achieved;
			}
			
			$manupulated_array = array(
				"month"=>$total_month,
				"channel"=>"All",
			"target"=>$total_target,
				"achieved"=>$total_achieved,
				"cumulated_target"=>$cum_total_target,
				"cumulated_achieved"=>$cum_achieved_current,
				"rv_target" => $total_rv_target
			);

			array_push($all, $manupulated_array);

			$total_target = $row["target"];
			$total_achieved = $row["achieved"];
			$cum_total_target += $row["target"];			
			$cum_total_achieved += $row["achieved"];
			$total_rv_target 	= $row["rv_target"];
			$cum_total_rv_target += $row["rv_target"];	

			$total_month = $row["month"];
			$i = 1;

		}
		


	//	var_dump($row["channel"]); echo '<br/>';


		if($row["channel"] == "KAM"){

			$cum_target_kam += $row["target"];	
				
			$cum_achieved_kam += $row["achieved"];
			
			$row["cumulated_target"] = $cum_target_kam;
			if($row["achieved"] == null){
				$row["cumulated_achieved"] 	= null;	
			}else{
				$row["cumulated_achieved"] 	= $cum_achieved_kam;
			}

			$cum_rv_target_kam += $row["rv_target"];	

			array_push($kam, $row);
		}else if($row["channel"] == "Agency"){
			$cum_target_agency += $row["target"];			
			$cum_achieved_agency += $row["achieved"];
			
			$row["cumulated_target"] = $cum_target_agency;
			if($row["achieved"] == null){
				$row["cumulated_achieved"] = null;	
			}else{
				$row["cumulated_achieved"] = $cum_achieved_agency;
			}
			$cum_rv_target_agency += $row["rv_target"];

			array_push($agency, $row);
		}else if($row["channel"] == "Broker"){
			$cum_target_broker += $row["target"];			
			$cum_achieved_broker += $row["achieved"];
			
			$row["cumulated_target"] = $cum_target_broker;
			if($row["achieved"] == null){
				$row["cumulated_achieved"] = null;	
			}else{
				$row["cumulated_achieved"] = $cum_achieved_broker;
			}
			$cum_rv_target_broker += $row["rv_target"];

			array_push($broker, $row);
		}else if($row["channel"] == "Digital"){
			$cum_target_digital += $row["target"];			
			$cum_achieved_digital += $row["achieved"];
			
			$row["cumulated_target"] = $cum_target_digital;
			if($row["achieved"] == null){
				$row["cumulated_achieved"] = null;	
			}else{
				$row["cumulated_achieved"] = $cum_achieved_digital;
			}
			$cum_rv_target_digital += $row["rv_target"];

			array_push($digital, $row);
		}else if($row["channel"] == "Softlogic"){
			$cum_target_softlogic += $row["target"];			
			$cum_achieved_softlogic += $row["achieved"];
			
			$row["cumulated_target"] = $cum_target_softlogic;
			if($row["achieved"] == null){
				$row["cumulated_achieved"] = null;	
			}else{
				$row["cumulated_achieved"] = $cum_achieved_softlogic;
			}
			$cum_rv_target_softlogic += $row["rv_target"];

			array_push($softlogic, $row);
		}else if($row["channel"] == "Affinity"){
			$cum_target_affinity += $row["target"];			
			$cum_achieved_affinity += $row["achieved"];
			
			$row["cumulated_target"] = $cum_target_affinity;
			if($row["achieved"] == null){
				$row["cumulated_achieved"] = null;	
			}else{
				$row["cumulated_achieved"] = $cum_achieved_affinity;
			}
			
			$cum_rv_target_affinity += $row["rv_target"];
			array_push($affinity, $row);	
		}else if($row["channel"] == "BNK"){
			$cum_target_banca += $row["target"];			
			$cum_achieved_banca += $row["achieved"];
		
			$row["cumulated_target"] = $cum_target_banca;
			if($row["achieved"] == null){
				$row["cumulated_achieved"] = null;	
			}else{
				$row["cumulated_achieved"] = $cum_achieved_banca;
			}

			$cum_rv_target_banca += $row["rv_target"];
			
			array_push($bnk, $row);

			
		}

		//print('<pre>'); print_r($bnk);exit;
		//print json_encode($row);
		//echo "</br>";
		
//		echo " : ";
//		echo $row["gwp"];
//		echo " : ";
//		echo $cum_target;
//		echo " : ";
//		echo $cum_gwp;
//		echo "</br>";
    }
	

} 

//exit;

		$cum_achieved_current = null;
			if($total_achieved != 0){
				$cum_achieved_current = $cum_total_achieved;
			}
		$manupulated_array = array(
				"month"=>$total_month,
				"channel"=>"All",
				"target"=>$total_target,
				"achieved"=>$total_achieved,
				"cumulated_target"=>$cum_total_target,
				"cumulated_achieved"=>$cum_achieved_current,
				"rv_target" => $total_rv_target
			);
		array_push($all, $manupulated_array);
		

//print json_encode($aa);

//print json_encode($bnk);

//echo "</br>~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~</br>";

$rows = array("All"=>$all, "KAM"=>$kam, "Broker"=>$broker, "Affinity"=>$affinity,"Agency"=>$agency, "Digital" => $digital, "Softlogic" => $softlogic,'BNK' => $bnk);
//array_push($rows, array("KAM"=>$kam));
//array_push($rows, array("Broker"=>$broker));
//array_push($rows, array("Affinity"=>$affinity));
//array_push($rows, array("Agency"=>$agency));

print json_encode($rows);
//print json_encode($kam);echo "</br>~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~</br>";
//print json_encode($agency);echo "</br>~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~</br>";
//print json_encode($broker);echo "</br>~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~</br>";
//print json_encode($affinity);echo "</br>~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~</br>";
$conn->close();
?>
