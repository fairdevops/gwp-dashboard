<?php
header("Access-Control-Allow-Origin: *");
// $servername = "172.24.10.45:3307";
// $username = "root";
// $password = "ace101";
// $dbname = "gwp_dashboard_aig";

$servername = "172.24.10.48";
$username = "gwp_user";
$password = "Gwp@123!";
$dbname = "gwp_dashboard_live"; 

	$jan = array();
	$feb = array();
	$mar = array();
	$apr = array();
	$may = array();
	$jun = array();
	$jul = array();
	$aug = array();
	$sep = array();
	$oct = array();
	$nov = array();
	$dec = array();

	$total_kam_achieved = 0;
	$total_kam_target = 0;
	$total_broker_achieved = 0;
	$total_broker_target = 0;
	$total_agency_achieved = 0;
	$total_agency_target = 0;
	$total_affinity_achieved = 0;
	$total_affinity_target = 0;
	$total_softlogic_target = 0;
	$total_softlogic_achieved=0;
	$total_digital_achieved = 0;
	$total_digital_target = 0;

	$total_banca_achieved = 0;
	$total_banca_target = 0;

// row processor
function processRow($raw_data){
	
	global $total_kam_target;
	global $total_kam_achieved;
	global $total_broker_target;
	global $total_broker_achieved;
	global $total_agency_target;
	global $total_agency_achieved;
	global $total_affinity_target;
	global $total_affinity_achieved;
	global $total_digital_target;
	global $total_digital_achieved;
	global $total_softlogic_target;
	global $total_softlogic_achieved;
	global $total_banca_target;
	global $total_banca_achieved;

	if($raw_data["channel"] == "KAM"){
		$total_kam_target += $raw_data["target"];
		$total_kam_achieved += $raw_data["achieved"];
	}else if($raw_data["channel"] == "Broker"){
		$total_broker_target += $raw_data["target"];
		$total_broker_achieved += $raw_data["achieved"];
	}else if($raw_data["channel"] == "Affinity"){
		$total_affinity_target += $raw_data["target"];
		$total_affinity_achieved += $raw_data["achieved"];
	}else if($raw_data["channel"] == "Agency"){
		$total_agency_target += $raw_data["target"];
		$total_agency_achieved += $raw_data["achieved"];
	}else if($raw_data["channel"] == "Digital"){
		$total_digital_target += $raw_data["target"];
		$total_digital_achieved += $raw_data["achieved"];
	}else if($raw_data["channel"] == "Softlogic"){
		$total_softlogic_target += $raw_data["target"];
		$total_softlogic_achieved += $raw_data["achieved"];
	}else if($raw_data["channel"] == "BNK"){
		$total_banca_target += $raw_data["target"];
		$total_banca_achieved += $raw_data["achieved"];
	}
	
	
	if($raw_data["month"] == "January"){
		global $jan;
		$result = array("target"=>$raw_data["target"],"achieved"=>$raw_data["achieved"]);
		$jan[$raw_data["channel"]] = $result;		
		//array_push($jan, array($raw_data["channel"]=>$result));		
	}else if($raw_data["month"] == "February"){
		global $feb;
		$result = array("target"=>$raw_data["target"],"achieved"=>$raw_data["achieved"]);
		$feb[$raw_data["channel"]] = $result;
	}else if($raw_data["month"] == "March"){
		global $mar;
		$result = array("target"=>$raw_data["target"],"achieved"=>$raw_data["achieved"]);
		$mar[$raw_data["channel"]] = $result;
	}else if($raw_data["month"] == "April"){
		global $apr;
		$result = array("target"=>$raw_data["target"],"achieved"=>$raw_data["achieved"]);
		$apr[$raw_data["channel"]] = $result;
	}else if($raw_data["month"] == "May"){
		global $may;
		$result = array("target"=>$raw_data["target"],"achieved"=>$raw_data["achieved"]);
		$may[$raw_data["channel"]] = $result;
	}else if($raw_data["month"] == "June"){
		global $jun;
		$result = array("target"=>$raw_data["target"],"achieved"=>$raw_data["achieved"]);
		$jun[$raw_data["channel"]] = $result;
	}else if($raw_data["month"] == "July"){
		global $jul;
		$result = array("target"=>$raw_data["target"],"achieved"=>$raw_data["achieved"]);
		$jul[$raw_data["channel"]] = $result;
	}else if($raw_data["month"] == "August"){
		global $aug;
		$result = array("target"=>$raw_data["target"],"achieved"=>$raw_data["achieved"]);
		$aug[$raw_data["channel"]] = $result;
	}else if($raw_data["month"] == "September"){
		global $sep;
		$result = array("target"=>$raw_data["target"],"achieved"=>$raw_data["achieved"]);
		$sep[$raw_data["channel"]] = $result;
	}else if($raw_data["month"] == "October"){
		global $oct;
		$result = array("target"=>$raw_data["target"],"achieved"=>$raw_data["achieved"]);
		$oct[$raw_data["channel"]] = $result;
	}else if($raw_data["month"] == "November"){
		global $nov;
		$result = array("target"=>$raw_data["target"],"achieved"=>$raw_data["achieved"]);
		$nov[$raw_data["channel"]] = $result;	
	}else if($raw_data["month"] == "December"){
		global $dec;
		$result = array("target"=>$raw_data["target"],"achieved"=>$raw_data["achieved"]);
		$dec[$raw_data["channel"]] = $result;
	}

}

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "
SELECT A.`month`, A.channel, A.target, SUM(B.achieved) AS achieved FROM 
(
SELECT `month`, channel, target as target FROM targets_with_revised 
) AS A 
LEFT JOIN 
(
SELECT 
CASE SUBSTR(TRDTGW,5,2) 
WHEN '01' THEN 'January' 
WHEN '02' THEN 'February' 
WHEN '03' THEN 'March' 
WHEN '04' THEN 'April' 
WHEN '05' THEN 'May' 
WHEN '06' THEN 'June' 
WHEN '07' THEN 'July' 
WHEN '08' THEN 'August' 
WHEN '09' THEN 'September' 
WHEN '10' THEN 'October' 
WHEN '11' THEN 'November' 
WHEN '12' THEN 'December' 
ELSE 'N/A' 
END as month, 

CASE PLCHGW 
WHEN 'AFN' THEN 'Affinity' 
WHEN 'AGT' THEN 'Agency' 
WHEN 'BRK' THEN 'Broker' 
WHEN 'KAM' THEN 'KAM'
WHEN 'SFL' THEN 'Softlogic' 
WHEN 'DGT' THEN 'Digital'
WHEN 'BNK' THEN 'BNK'
ELSE 'N/A'
END as channel

, ROUND(SUM(GWPAGW),2) as achieved 
FROM gwp_source 

GROUP BY SUBSTR(TRDTGW,5,2), PLCHGW 

) as B
ON B.`month` = A.`month`
AND B.channel = A.channel 
GROUP BY `month`,channel 
ORDER BY FIELD(A.`month`,'January','February','March','April','May','June','July','August','September','October','November','December')

";
$result = $conn->query($sql);

	


if ($result->num_rows > 0) {
    // output data of each row
	$rows = array();
	
	while($row = $result->fetch_assoc()) {
		//print json_encode($row);	
		processRow($row);
		//echo "</br>";
    }
	
	global $jan;
	global $feb;
	global $mar;
	global $apr;
	global $may;
	global $jun;
	global $jul;
	global $aug;
	global $sep;
	global $oct;
	global $nov;
	global $dec;
	
	
//	array_push($rows, array("January"=>$jan));
//	array_push($rows, array("February"=>$feb));
//	array_push($rows, array("March"=>$mar));
//	array_push($rows, array("April"=>$apr));
//	array_push($rows, array("May"=>$may));
//	array_push($rows, array("June"=>$jun));
//	array_push($rows, array("July"=>$jul));
//	array_push($rows, array("August"=>$aug));
//	array_push($rows, array("September"=>$sep));
//	array_push($rows, array("October"=>$oct));
//	array_push($rows, array("November"=>$nov));
//	array_push($rows, array("December"=>$dec));
	
	global $total_kam_target;
	global $total_kam_achieved;
	global $total_broker_target;
	global $total_broker_achieved;
	global $total_agency_target;
	global $total_agency_achieved;
	global $total_affinity_target;
	global $total_affinity_achieved;
	global $total_affinity_target;
	global $total_affinity_achieved;

		global $total_digital_achieved;
	global $total_digital_target;
		global $total_softlogic_achieved;
	global $total_softlogic_target;
	
	global $total_banca_target;
		global $total_banca_achieved;
	
	//array("target"=>$total_kam_target,"achieved"=>$total_kam_achieved);
	// array_push($rows, array("All"=>array(
	// 	"KAM"=>array("target"=>$total_kam_target,"achieved"=>$total_kam_achieved),
	// 	"Broker"=>array("target"=>$total_broker_target,"achieved"=>$total_broker_achieved),
	// 	"Agency"=>array("target"=>$total_agency_target,"achieved"=>$total_agency_achieved),
	// 	"Affinity"=>array("target"=>$total_affinity_target,"achieved"=>$total_affinity_achieved),
	// 	"Digital"=>array("target"=>$total_digital_target,"achieved"=>$total_digital_achieved),
	// 	"Softlogic"=>array("target"=>$total_softlogic_target,"achieved"=>$total_softlogic_achieved)		
	// )));
	
	//var_dump($total_banca_target); exit;
	$rows = array(
		"January"=>$jan,
		"February"=>$feb,
		"March"=>$mar,
		"April"=>$apr,
		"May"=>$may,
		"June"=>$jun,
		"July"=>$jul,
		"August"=>$aug,
		"September"=>$sep,
		"October"=>$oct,
		"November"=>$nov,
		"December"=>$dec,
		"All"=>array(
			"KAM"=>array("target"=>$total_kam_target,"achieved"=>$total_kam_achieved),
			"Broker"=>array("target"=>$total_broker_target,"achieved"=>$total_broker_achieved),
			"Agency"=>array("target"=>$total_agency_target,"achieved"=>$total_agency_achieved),
			"Affinity"=>array("target"=>$total_affinity_target,"achieved"=>$total_affinity_achieved),
			"Digital"=>array("target"=>$total_digital_target,"achieved"=>$total_digital_achieved),
			"Softlogic"=>array("target"=>$total_softlogic_target,"achieved"=>$total_softlogic_achieved),
			"BNK"=>array("target"=>$total_banca_target,"achieved"=>$total_banca_achieved)
		)
	);
//	$all;
	//$result = array("target"=>$raw_data["target"],"achieved"=>$raw_data["achieved"]);
//	array_push($all, array($raw_data["channel"]=>$result));	
	
	//print json_encode($jan);
	//print_r($jan);
	
} 
print json_encode($rows);
$conn->close();


?>
