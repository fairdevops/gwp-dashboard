<?php
header("Access-Control-Allow-Origin: *");
// $servername = "172.24.10.45:3307";
// $username = "root";
// $password = "ace101";
// $dbname = "gwp_dashboard_aig";

$servername = "172.24.10.48";
$username = "gwp_user";
$password = "Gwp@123!";
$dbname = "gwp_dashboard_live";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "
SELECT * FROM 
(
(
SELECT * FROM 
(
SELECT 

CASE SUBSTR(TRDTGW,5,2)
WHEN '01' THEN 'January' 
WHEN '02' THEN 'February' 
WHEN '03' THEN 'March' 
WHEN '04' THEN 'April' 
WHEN '05' THEN 'May' 
WHEN '06' THEN 'June' 
WHEN '07' THEN 'July' 
WHEN '08' THEN 'August' 
WHEN '09' THEN 'September' 
WHEN '10' THEN 'October' 
WHEN '11' THEN 'November' 
WHEN '12' THEN 'December' 
ELSE 'N/A' 
END AS `month`,
CASE PLCHGW 
WHEN 'AFN' THEN 'Affinity' 
WHEN 'AGT' THEN 'Agency' 
WHEN 'BRK' THEN 'Broker' 
WHEN 'KAM' THEN 'KAM' 
WHEN 'SFL' THEN 'Softlogic' 
WHEN 'DGT' THEN 'Digital' 
WHEN 'BNK' THEN 'BNK'
ELSE 'N/A'
END as channel,  
IF (SUBSTR(PLNOGW, 3, 1) = 'M','Motor','NonMotor') as category, 
ROUND(SUM(GWPAGW),2) as gwp 
FROM gwp_source
GROUP BY SUBSTR(PLNOGW, 3, 1), SUBSTR(TRDTGW,5,2), PLCHGW
) AS A GROUP BY category,`month`,channel 
)
UNION ALL
(
/* Staging yard ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
SELECT A.`month`, A.channel, A.category, SUM(A.gwp) AS achieved FROM 
(
SELECT 
CASE SUBSTR(TRDTGW,5,2)
WHEN '01' THEN 'January' 
WHEN '02' THEN 'February' 
WHEN '03' THEN 'March' 
WHEN '04' THEN 'April' 
WHEN '05' THEN 'May' 
WHEN '06' THEN 'June' 
WHEN '07' THEN 'July' 
WHEN '08' THEN 'August' 
WHEN '09' THEN 'September' 
WHEN '10' THEN 'October' 
WHEN '11' THEN 'November' 
WHEN '12' THEN 'December' 
ELSE 'N/A' 
END AS `month`, 
CASE PLCHGW 
WHEN 'AFN' THEN 'Affinity' 
WHEN 'AGT' THEN 'Agency' 
WHEN 'BRK' THEN 'Broker' 
WHEN 'KAM' THEN 'KAM' 
WHEN 'SFL' THEN 'Softlogic' 
WHEN 'DGT' THEN 'Digital' 
WHEN 'BNK' THEN 'BNK'
ELSE 'N/A'
END as channel, 
IF (RNCTGW = 999,'NewBusiness','Renewal') AS category,
GWPAGW as A, 
ROUND(SUM(GWPAGW),2) as gwp, 
COUNT(GWPAGW) as records

FROM gwp_source 

GROUP BY RNCTGW, SUBSTR(TRDTGW,5,2), PLCHGW

) AS A GROUP BY `month`,category, channel 
)
) AS C
ORDER BY FIELD(`month`,'January','February','March','April','May','June','July','August','September','October','November','December')
;
";


$sql = "
SELECT 
CASE `month` 
WHEN '01' THEN 'January' 
WHEN '02' THEN 'February' 
WHEN '03' THEN 'March' 
WHEN '04' THEN 'April' 
WHEN '05' THEN 'May' 
WHEN '06' THEN 'June' 
WHEN '07' THEN 'July' 
WHEN '08' THEN 'August' 
WHEN '09' THEN 'September' 
WHEN '10' THEN 'October' 
WHEN '11' THEN 'November' 
WHEN '12' THEN 'December' 
ELSE 'N/A' 
END AS `month`, 
CASE channel 
WHEN 'AFN' THEN 'Affinity' 
WHEN 'AGT' THEN 'Agency' 
WHEN 'BRK' THEN 'Broker' 
WHEN 'KAM' THEN 'KAM' 
WHEN 'SFL' THEN 'Softlogic' 
WHEN 'DGT' THEN 'Digital' 
WHEN 'BNK' THEN 'BNK'
ELSE 'N/A'
END as channel, 
category, 
gwp 
FROM 
(
SELECT `month`, channel, category, SUM(gwp) as gwp  FROM (
SELECT 
SUBSTR(TRDTGW,5,2) as `month`, 
PLCHGW as `channel`,  
IF (SUBSTR(PLNOGW, 3, 1) = 'M','Motor','NonMotor') as category, 
ROUND(SUM(GWPAGW),2) as gwp 
FROM gwp_source 
GROUP BY SUBSTR(TRDTGW,5,2), PLCHGW, SUBSTR(PLNOGW, 3, 1)
) AS A GROUP BY `month`, channel, category 
UNION ALL 
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
SELECT A.`month`, A.channel, A.category, SUM(A.gwp) as gwp FROM 
(
SELECT 
SUBSTR(TRDTGW,5,2) AS `month`, 
PLCHGW AS channel, 
IF (RNCTGW = 999,'NewBusiness','Renewal') AS category,
GWPAGW as A, 
ROUND(SUM(GWPAGW),2) as gwp 
FROM gwp_source 
GROUP BY RNCTGW, SUBSTR(TRDTGW,5,2), PLCHGW
) AS A GROUP BY `month`,category, channel 
) AS A ORDER BY  FIELD(`month`,'January','February','March','April','May','June','July','August','September','October','November','December')
";

$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
	$rows = array();
	/*
	$agency_total_nb = 0;
	$agency_total_rn = 0;
	$agency_total_mo = 0;
	$agency_total_nm = 0;
    */
	$broker_total_nb = 0;
	$broker_total_rn = 0;
	$broker_total_mo = 0;
	$broker_total_nm = 0;
	$kam_total_nb = 0;
	$kam_total_rn = 0;
	$kam_total_mo = 0;
	$kam_total_nm = 0;
	$affinity_total_nb = 0;
	$affinity_total_rn = 0;
	$affinity_total_mo = 0;
	$affinity_total_nm = 0;
	
    while($row = $result->fetch_assoc()) {
		$rows[$row["month"]][$row["channel"]][$row["category"]] = $row["gwp"];
		if( isset($rows["All"][$row["channel"]][$row["category"]]) ){
			$rows["All"][$row["channel"]][$row["category"]] += $row["gwp"];
		}else{
			$rows["All"][$row["channel"]][$row["category"]] = $row["gwp"];
		}
    }
	
		
} 
print json_encode($rows);
$conn->close();
?>
