<?php
header("Access-Control-Allow-Origin: *");
$servername = "172.24.10.48";
$username = "gwp_user";
$password = "Gwp@123!";
$dbname = "gwp_dashboard_live";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "

SELECT portfolio_targets.*, B.achieved FROM 
portfolio_targets 
LEFT JOIN 
(
SELECT A.month, SUM(A.count), business_class_portfolio.portfolio AS portfolio, 
A.portfolio as business_class, 
SUM(A.achieved) as achieved FROM 
(
SELECT 
COUNT(GWPAGW) as count, 
CASE SUBSTR(TRDTGW,5,2) 
WHEN '01' THEN 'January' 
WHEN '02' THEN 'February' 
WHEN '03' THEN 'March' 
WHEN '04' THEN 'April' 
WHEN '05' THEN 'May' 
WHEN '06' THEN 'June' 
WHEN '07' THEN 'July' 
WHEN '08' THEN 'August' 
WHEN '09' THEN 'September' 
WHEN '10' THEN 'October' 
WHEN '11' THEN 'November' 
WHEN '12' THEN 'December' 
ELSE 'N/A' 
END as `month`, 
BSCLGW as portfolio, 
ROUND(SUM(GWPAGW),2) as achieved 
FROM gwp_source 
GROUP BY SUBSTR(TRDTGW,5,2), BSCLGW 

) AS A 
LEFT JOIN business_class_portfolio 
ON business_class_portfolio.business_class = A.portfolio 
GROUP BY A.`month`, business_class_portfolio.portfolio 
)
AS B ON B.`month` = portfolio_targets.`month` AND B.portfolio = portfolio_targets.portfolio 

;

";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
	$rows = array();
    
    $total_motor_achieved = 0;
    $total_fire_achieved = 0;
    $total_health_achieved = 0;
    $total_casualty_achieved = 0;
    $total_marine_achieved = 0;
    
    $total_motor_target = 0;
    $total_fire_target = 0;
    $total_health_target = 0;
    $total_casualty_target = 0;
    $total_marine_target = 0;
    
    while($row = $result->fetch_assoc()) {
        $rows[$row['month']][] = $row;
        if($row['portfolio'] == 'Motor'){
            $total_motor_achieved += $row['achieved'];
            $total_motor_target += $row['target'];
        } else if($row['portfolio'] == 'Health'){
            $total_health_achieved += $row['achieved'];
            $total_health_target += $row['target'];
        } else if($row['portfolio'] == 'Casualty'){
            $total_casualty_achieved += $row['achieved'];
            $total_casualty_target += $row['target'];
        } else if($row['portfolio'] == 'Marine'){
            $total_marine_achieved += $row['achieved'];
            $total_marine_target += $row['target'];
        } else if($row['portfolio'] == 'Fire'){
            $total_fire_achieved += $row['achieved'];
            $total_fire_target += $row['target'];
        }
    }

/**
    if($total_motor_target == 0){
        $total_motor_target = $total_motor_achieved;
    }

    if($total_fire_target == 0){
        $total_fire_target = $total_fire_achieved;
    }

    if($total_casualty_target == 0){
        $total_casualty_target = $total_casualty_achieved;
    }

    if($total_marine_target == 0){
        $total_marine_target = $total_marine_achieved;
    }
*/

    $rows["All"][] = array("month"=>"All","portfolio"=>"Motor","target"=>$total_motor_target,"achieved"=>$total_motor_achieved);
    $rows["All"][] = array("month"=>"All","portfolio"=>"Fire","target"=>$total_fire_target,"achieved"=>$total_fire_achieved);
    $rows["All"][] = array("month"=>"All","portfolio"=>"Casualty","target"=>$total_casualty_target,"achieved"=>$total_casualty_achieved);
    $rows["All"][] = array("month"=>"All","portfolio"=>"Health","target"=>$total_health_target,"achieved"=>$total_health_achieved);
    $rows["All"][] = array("month"=>"All","portfolio"=>"Marine","target"=>$total_marine_target,"achieved"=>$total_marine_achieved);

} 

print json_encode($rows);

$conn->close();
?>
